package JAVA_Fundamentals.Leksion2;

/**
 * Shkruani nje program ne Java qe do te shkembej vlerat e dy variablave
 * (duke perdorur nje variable te trete ndihmes).
 */
public class DSH1 {
    public static void main(String[]args){
        int a = 10, b = 5, c;       // variabli c eshte pikerisht variabli ndihmes
        c=a;
        a=b;
        b=c;
        System.out.println("variabli a eshte: " +a);
        System.out.println("variabli  b eshte: " +b);
    }
}
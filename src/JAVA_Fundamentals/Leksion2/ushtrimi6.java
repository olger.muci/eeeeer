package JAVA_Fundamentals.Leksion2;
import java.util.Scanner;
/**
 * Write a Java program to take three numbers from the user and print the greatest number.
*/
public class ushtrimi6 {
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);
        System.out.print("Vendos nr. e pare: ");
        int nr1 = input.nextInt();
        System.out.print("Vendos nr. e dyte: ");
        int nr2 = input.nextInt();
        System.out.print("Vendos nr. e trete: ");
        int nr3 = input.nextInt();
        if (nr1 > nr2 && nr1 > nr3) {
            System.out.println("Numri me i madh eshte: " + nr1);
        } else if (nr2 > nr1 && nr2 > nr3) {
            System.out.println("Numri me i madh eshte: " + nr2);
        } else if (nr3 > nr1 && nr3 > nr2) {
            System.out.println("Numri me i madh eshte: " + nr3);
        } else {
            System.out.println("Numrat jane te barabarte.");
        }
    }
}
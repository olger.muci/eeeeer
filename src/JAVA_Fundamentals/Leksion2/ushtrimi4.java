package JAVA_Fundamentals.Leksion2;

import java.util.Scanner;

/**
 *  Write a program called which prints "Odd Number"
 *  if the int variable "number" is odd, or "Even Number" otherwise.
 *  The program shall always print "bye!" before exiting.
 */
public class ushtrimi4 {
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);
        System.out.print("Shkruaj nje numer: ");
        int number = input.nextInt();
        if (number % 2 == 0){
            System.out.println("Even Number");
        }else{
            System.out.println("Odd Number");
        }
        System.out.println("bye!");
    }
}
package JAVA_Fundamentals.Leksion2;

/**
 *  Write a program to calculate the sum of the first and the second last digit of a 5 digit.
 *  E.g.- NUMBER: 12345 OUTPUT: 1+4=5
 */
public class ushtrimi1 {
    public static void main(String[]args){
        int a = 12345;
       int nr1 = a/10000;
        int nrParafundit = a/10%10;
        System.out.println("Shuma eshte: " + (nr1+nrParafundit));
    }
}
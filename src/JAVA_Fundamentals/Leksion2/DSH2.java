package JAVA_Fundamentals.Leksion2;

/**
 * Shkruani nje program ne Java qe do te shkembej vlerat e dy variablave
 * (pa perdorur nje variable te trete).
 */
public class DSH2 {
    public static void main(String[]args){
        int a=10;
        int b=5;
        a=a+b;      // si fillim a do e kete vleren 10+5=15
        b=a-b;      // b do e kete vleren 15-5=10, dmth e beme ndryshimin
        a=a-b;      // perfundimisht a do e kete vleren 15-10=5
        System.out.println("variabli a eshte:" +a);
        System.out.println("variabli b eshte:" +b);
    }
}
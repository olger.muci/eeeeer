package JAVA_Fundamentals.Leksion2;

/**
 *  Write a program to reverse a 3-digit number.
 *  E.g.-Number: 132, Output: 231
 */
public class ushtrimi2 {
    public static void main(String []args){
        int nr = 789;
        int a = nr%10;
        int b = nr/10%10;
        int c = nr/100;
        a=a*100;
        b=b*10;
        int nrReverse = a+b+c;
        System.out.println("Numri i kthyer eshte: " + nrReverse);
    }
}
package JAVA_Fundamentals.Leksion2;

import java.util.Scanner;

/**
 * Write a program called which prints "PASS" if the int variable "mark"
 *    is more than or equal to 50; or prints "FAIL" otherwise.
 *    The program shall always print "DONE" before exiting.
 */
public class ushtrimi3 {
    public static void main(String []args){
        Scanner input= new Scanner(System.in);
        System.out.print("Sa pike ke marr ne provim? ");
        int mark= input.nextInt();
        if (mark >= 50) {
            System.out.println("PASS");
        } else {
            System.out.println("FAIL");
        }
    }
}
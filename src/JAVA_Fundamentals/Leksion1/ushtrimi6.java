package JAVA_Fundamentals.Leksion1;

import java.util.Scanner;

/**
 * Declare a variable corresponding to age; use a ternary operator to display: adult or underage.
 */
public class ushtrimi6 {
    public static void main(String[]args){
        Scanner input= new Scanner(System.in);
        System.out.print("Sa vjec je? ");
        int age= input.nextInt();
        boolean isAdult = age >= 18;
        String ageName = isAdult ? "Adult" : "Underage";
        System.out.println(ageName);
    }
}
package JAVA_Fundamentals.Leksion1;

/**
 * Assign values of variables 'a' and 'b' and then check if both the conditions 'a < 50' and 'a < b' are true.
 */
public class ushtrimi5 {
    public static void main(String[]args){
        int a = 10;
        int b = 15;
        boolean myBoolValue = a<50 && a<b;
        System.out.println(myBoolValue);
    }
}
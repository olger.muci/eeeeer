package JAVA_Fundamentals.Leksion1;
import java.util.Scanner;
/**
 * Write a Java program to print the area and perimeter of a rectangle.
 */
public class ushtrimi4 {
    public static void main(String []args){
        Scanner input = new Scanner(System.in);
        System.out.print("Percakto brinjen a: ");
        int brinjaA = input.nextInt();
        System.out.print("Percakto brinjen b: ");
        int brinjaB = input.nextInt();
        System.out.println("Syprina eshte: " + (brinjaA * brinjaB)+" cm^2.");
        System.out.print("Perimetri eshte: " +(2*brinjaA + 2*brinjaB)+ " cm.");
    }
}
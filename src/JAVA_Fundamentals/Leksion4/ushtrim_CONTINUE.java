package JAVA_Fundamentals.Leksion4;

/**
 * ushtrim me continue (afishimi i disa numrave)
 */
public class ushtrim_CONTINUE {
    public static void main(String[]args){
        for (int i=1; i<=20; i++) {
            if (i>=13 && i<=15) {
                continue;     // pra cikli vazhdon te funksionoj duke perjashtuar kushtin e percaktuar
            }
            System.out.print(i + " ");
        }
    }
}
package JAVA_Fundamentals.Leksion4;

import java.util.Scanner;

/**
 * Ndertoni nje METODE ne java qe gjen nese nje numer eshte cift ose tek.
 */

public class Ushtrimi4 {
    public static void main(String []args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Vendos nje numer: ");
        int nr = input.nextInt();
        System.out.println(isEven(nr));
    }
    public static boolean isEven(int nr){
        if (nr%2==0){
           return true;
        }
        return false;
    }
}
package JAVA_Fundamentals.Leksion4;

/**
 * ushtrim me break (shuma e disa numrave dhe sasia e nr qe jane mbledhur me njeri-tjetrin)
 */
public class ushtrim_BREAK {
    public static void main(String[]args){
        int shuma=0,counter=0;                          // counter=variabel qe do te numeroj numrat qe mblidhen
        for (int i=1; i<=20; i++) {
            shuma= shuma+i;
            counter++;
            if (shuma >= 50) {
                break;          // pra cikli pushon se funksionuari ne momentin qe plotesohet nje kusht i caktuar brenda ciklit
            }
        }
        System.out.println("Shuma eshte: " + shuma);
        System.out.println("Sasia e numrave te mbledhur eshte: " + counter);
    }
}
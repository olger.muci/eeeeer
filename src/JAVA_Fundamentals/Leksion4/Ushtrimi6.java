package JAVA_Fundamentals.Leksion4;
import java.util.Scanner;
/**
 * Shkruani nje METODE ne java qe gjen mesataren arithmetike te tre numrave.
 */
public class Ushtrimi6 {
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);

        System.out.print("Shkrauni numrin e pare: ");
        int nr1 = input.nextInt();

        System.out.print("Shkruani numrin e dyte: ");
        int nr2 = input.nextInt();

        System.out.print("Shkrauni numrin e trete: ");
        int nr3 = input.nextInt();

        System.out.printf("Mesatarja eshte: %.1f ", findAverageOf3Numbers(nr1, nr2, nr3));
    }

    public static double findAverageOf3Numbers(int nr1, int nr2, int nr3) {
        return (double) (nr1+nr2+nr3)/3;
    }
}
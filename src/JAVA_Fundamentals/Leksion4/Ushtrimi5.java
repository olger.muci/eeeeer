package JAVA_Fundamentals.Leksion4;

/**
 *  Shkruani nje program ne java qe gjen vlerat e dublikuara ne nje array te dhene.
 */
public class Ushtrimi5 {
    public static void main(String[]args){
        int[] Array = {1, 2, 5, 5, 6, 6, 7, 2};
        for(int i=0; i<Array.length-1; i++) {
            for(int j=i+1; j<Array.length; j++) {
                if((Array[i]==Array[j]) && (i!=j)){
                    System.out.println("Vlera e dublikuar eshte: "+Array[j]);
                }
            }
        }
    }
}
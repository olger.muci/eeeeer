package JAVA_Fundamentals.Leksion4;

import java.util.Scanner;

/**
 * shkruani nje METODE qe teston nese nje element i dhene eshte ose jo pjese e nje array te dhene.
 */
public class Ushtrimi2 {
    public static void main(String[]args){
        Scanner input= new Scanner(System.in);
        int[] Array = new int[]{1,2,3,4,5};
        System.out.print("Shkruaj elementin qe do te testosh: ");
        int element = input.nextInt();
        System.out.println(ArrayPermbanElementin(Array, element));
    }
    public static boolean ArrayPermbanElementin(int[]Array, int element) {
        for(int i=0; i<Array.length; i++) {
            if(element==Array[i]){
                return true;        // dmth e permban
            }
        }
        return false;           // nuk e permban
    }
}
package JAVA_Fundamentals.Leksion4;

/**
 *Shkruani programin ne java qe gjen max dhe min e vlerave ne nje array.
 *
 */
public class Ushtrimi1 {
    public static void main(String[] args) {
        int[] Array = new int[]{1, 7, 10, 4, 5, 9, 6};
        int min=Array[0];      // si minimum dhe max e marrim vete psh Array[0] dhe gjithe vlerat e tjera do ti krahasojme me kete
        int max=Array[0];
        for (int i=0; i<Array.length; i++) {
            if (min>Array[i]) {
                min = Array[i];
            }
            if (max<Array[i]){
                max=Array[i];
            }
        }
        System.out.println("Minimumi eshte: " +min);
        System.out.print("Maximumi eshte: " +max);
    }
}
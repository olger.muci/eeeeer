package JAVA_Fundamentals.Leksion4.CLASSES;
public class Book {
    private String title;
    private String author;
    private int numberOfPages;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {                 // metoda getter perdoret per te marr nje vlere qe
                                                // kemi dhene permes setter edhe kur fields e klases jane private
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        if (isNumberOfPagesCorrect(numberOfPages)) {
            this.numberOfPages = numberOfPages;
        } else {
            System.out.println("The provided number of pages is incorrect: " + numberOfPages);
        }
    }

    private boolean isNumberOfPagesCorrect(int numberOfPages) {
        return numberOfPages > 0;
    }
}
package JAVA_Fundamentals.Leksion4.CLASSES;

/**
 *  Krijoni nje Klase Dog qe:
 *  ka 3 properties (class fields)  breed; age; color;
 *  Shkruani getter dhe setter perkates, si dhe behaviours (class method) bark and sleep.
 */
public class DOG {

        private String breed;           // field no.1
        private int age;                // field no.2
        private String color;           // field no.3

         public String getBreed() {
            return breed;
         }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public void bark () {                                               // metoda bark
            System.out.println("woof - woof");
        }

        public void sleep () {                                              // metoda sleep
            System.out.println("zzz - zzz");
        }

//         public void dogParameters(){
//            System.out.println(String
//                                    .format("Mosha e qenit eshte: %d, Rraca eshte: %s, Ngjyra eshte: %s",age,breed,color));
//        }
//
//    public static void main(String[] args) {
//        DOG dog1 =new DOG();
//        DOG dog2= new DOG();
//
//        dog1.setAge(12);
//        dog1.setBreed("pitbull");
//        dog1.setColor("black");
//        dog1.bark();
//        dog1.sleep();
//        dog1.dogParameters();
//
//        dog2.setAge(8);
//        dog2.setBreed("doberman");
//        dog2.setColor("gri");
//        dog2.bark();
//        dog2.sleep();
//        dog2.dogParameters();
//    }
}
package JAVA_Fundamentals.Leksion4.CLASSES;

public class Car {
    private String color;
    private int maxSpeed;
    private String brand;

    public void setColor(String color) {     // metoda setter perdoret per te vendosur nje vlere konkrete permes .this
        this.color = color;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void printCarParameters() {
        System.out.println(String
                                .format("Car color is: %s, max speed is: %d, car brand is: %s", color, maxSpeed, brand));
    }

    public static void main(String[] args) {
        Car car1= new Car();

        car1.setMaxSpeed(120);
        car1.setColor("blu");
        car1.setBrand("polo");
        car1.printCarParameters();

        Car car2=new Car();

        car2.setMaxSpeed(240);
        car2.setBrand("benz");
        car2.setColor("white");
        car2.printCarParameters();
    }
}
package JAVA_Fundamentals.Leksion4.CLASSES;

public class BookTest {
    public static void main(String[] args) {
        Book testBook = new Book();
        testBook.setAuthor("Ismail KADARE");
        testBook.setTitle("KESHTJELLA");
        testBook.setNumberOfPages(5);

        System.out.println("Titulli eshte: " + testBook.getTitle());
        System.out.println("Autori eshte: " + testBook.getAuthor());

        System.out.println("Numri i faqeve eshte: " + testBook.getNumberOfPages());
    }
}
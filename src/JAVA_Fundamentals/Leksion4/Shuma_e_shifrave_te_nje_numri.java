package JAVA_Fundamentals.Leksion4;
/**
 * Shkruani programin ne java qe do te gjej shumen e shifrave te nje numri te dhene.
 * Psh: 123 => 1+2+3=6
 */

import java.util.Scanner;

public class Shuma_e_shifrave_te_nje_numri {
    public static void main(String[] args){
        int numri,mbetja,shuma=0;
        Scanner input= new Scanner(System.in);
        System.out.print("Shkruaj nje numer: ");
        numri= input.nextInt();
        while(numri!=0){
            mbetja=numri%10;
            shuma=shuma+mbetja;
            numri=numri/10;
        }
        System.out.print("Shuma e shifrave eshte: " + shuma);
    }
}
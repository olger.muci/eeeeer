package JAVA_Fundamentals.Leksion4;

import java.util.Scanner;

public class Metode_per_nr_çift {
    public static void main(String[] args) {

        Scanner input= new Scanner(System.in);
        System.out.print("Shkruaj nje numer: ");
        int number = input.nextInt();
        System.out.println(isEven(number));
    }
    public static boolean isEven(int number) {
        return (number % 2 == 0) ? true : false;        // ternary operation
    }
}
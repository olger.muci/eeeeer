package JAVA_Fundamentals.Leksion4;

/**
 *
 * Shkruaj programin ne java qe do te mbledhi 2 matrica (array 2-dimensionale) me te njejtin dimension.
 */
public class Ushtrimi3 {
    public static void main(String []args){
    int[][] matrica1= new int[][]{      {1,2},
                                        {3,4}
                                                };

    int[][] matrica2= new int[][]{      {5,6},
                                        {7,8}
                                                };
    int[][] matrica3= new int[2][2];
    for (int i=0; i<matrica1.length;i++){
        for (int j=0; j<matrica1[i].length;j++){
           matrica3[i][j]=matrica1[i][j]+matrica2[i][j];
            System.out.print(matrica3[i][j]+" ");
        }
        System.out.println();
    }
    }
}
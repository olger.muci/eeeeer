package JAVA_Fundamentals.Leksion4;
import java.util.Scanner;
/**
 * Shkruani nje METODE ne java qe gjen numrin me te vogel midis tri numrave te dhene.
 */
public class Ushtrimi7 {
    public static void main(String[]args){

        Scanner input = new Scanner(System.in);

        System.out.print("Shkruani numrin e pare: ");
        int number1 = input.nextInt();

        System.out.print("Shkruani numrin e dyte: ");
        int number2 = input.nextInt();

        System.out.print("Shkruani numrin e trete: ");
        int number3 = input.nextInt();

        System.out.println("Numri me i vogel eshte: " + findMin(number1, number2, number3));

    }

    public static int findMin(int number1, int number2, int number3){
        if(number1<number2 && number1<number3){
            return number1;
        }
        else if(number2<number1 && number2<number3) {
            return number2;
        }
        else if (number3<number1 && number3<number2){
            return number3;
        }
        else {
            return -9999; // me marrveshje do afishohet -9999 kur numrat jane te barabarte
        }
    }
}
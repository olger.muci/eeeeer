package JAVA_Fundamentals.Leksion4;

import java.util.Scanner;

public class Metode_per_shumen_e_shifrave_te_nje_nr {

    public static int findSumOfDigit(int nr){
        int shuma=0;
        while (nr!=0){
            shuma=shuma+nr%10;
            nr=nr/10;
        }
        return shuma;
    }

    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        System.out.print("Shkruaj nje numer: ");
        int nr= input.nextInt();
        System.out.println("Shuma e shifrave eshte: "+findSumOfDigit(nr));
    }
}
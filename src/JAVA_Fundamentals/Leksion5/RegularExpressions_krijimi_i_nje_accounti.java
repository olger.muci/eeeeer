package JAVA_Fundamentals.Leksion5;
/**
 * regular expressions
 */

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressions_krijimi_i_nje_accounti {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Vendos emrin tend: ");
        String name = scanner.nextLine();

        if (validateName(name)) {
            System.out.print("Vendos passwordin: ");
            String password = scanner.nextLine();

            if (validatePassword(password)) {
                System.out.println("Regjistrimi u krye me sukses");
            } else {
                System.out.println("Passwordi i vendosur nuk eshte ne formatin e duhur, programi ndalon");
            }

        } else {
            System.out.println("Emri i vendosur nuk eshte ne formatin e duhur, programi ndalon");
        }
    }
    public static boolean validateName(String name) {
        String regex = "[A-Z][a-z]+";           // Shkronja e pare duhet te jete kapitale dhe gjithe te tjerat te vogla.

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(name);

        return matcher.matches();
    }

    public static boolean validatePassword(String password) {
        String regex = "^(?=.*[0-9])"           // Te pakten nje numer nga 0 deri tek 9
                + "(?=.*[a-z])(?=.*[A-Z])"      // Te pakten nje germe te vogel dhe nje germe te madhe
                + "(?=.*[@#$%^&+=])"            // Te pakten nje nga keto karakteret @; #; $; %; ^; &; +; =.
                + "(?=\\S+$).{8,20}$";          // Pa hapesira dhe te kete gajtesi 8-20 karaktere.

        Pattern pattern1 = Pattern.compile(regex);
        Matcher matcher1 = pattern1.matcher(password);

        return matcher1.matches();
    }
}
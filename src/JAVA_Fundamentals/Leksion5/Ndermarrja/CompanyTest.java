package JAVA_Fundamentals.Leksion5.Ndermarrja;

/**
 * Create a test class named CompanyTest:
 *
 *          a)  In the main() method, create 5 instances of Employee type,
 *              which will be a representation of 5 employees working in the company.
 *              Then add them to the table and create an instance of type Company using one-argument constructor.
 *
 *          b)  Set employees field values using previously created setter.
 *
 *          c)  Display all company data on the standard output
 *              and average age computed by calling getAverageAge() method on Company type instance.
 */

public class CompanyTest {
    public static void main(String[] args) {
        Employee[] employees = new Employee[]{
                new Employee("John", "Doe", 35),
                new Employee("Kate", "Doe", 25),
                new Employee("Mark", "Doe", 29),
                new Employee("Ann", "Doe", 12),
                new Employee("Richard", "Doe", 57)
        };

        Company company = new Company("X CAPITAL");

        company.setEmployees(employees);

        System.out.println("Company data: " + company);

        System.out.println("Mosha mesatare e punetorve eshte: " + company.getAverageAge());
    }
}
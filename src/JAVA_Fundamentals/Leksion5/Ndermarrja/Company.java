package JAVA_Fundamentals.Leksion5.Ndermarrja;
import java.util.Arrays;

/**
 * In class Company create fields:
 *        a)    name of String type - company name
 *        b)    employees, which is a 5-element table of Employee type (Employee[]) - table will store all company employees
 *        c)    add one-argument constructor, which takes name parameter as an argument
 *        d)    create setEmployees() method definition for setting company employees.
 *        e)    implement getAverageAge(), which returns the average age of company employees.
 *        f)    implement toString() methods for every class which will be returning whole object data.
 */
public class Company {
    private String name;    // emri kompanise
    private Employee[] employees = new Employee[5];
    public Company(String name) {
        this.name = name;
    }
    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }
    public float getAverageAge() {
        float sum = 0;
        for (int i = 0; i < employees.length; i++) {
            sum = sum + employees[i].getAge();
        }
        return sum / employees.length;
    }
    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", employees=" + Arrays.toString(employees) +
                '}';
    }
}
package JAVA_Fundamentals.Leksion5.Ndermarrja;

/**
 * Implement a class named Employee, which:
 *          a)   consists of three fields: firstName, lastName and age.
 *          b)   add a three-argument constructor, which allows initializing field values and getter for age field.
 */
public class Employee {         // klasa e punetoreve
    private String firstName;
    private String lastName;
    private int age;
    public Employee(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    public Integer getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}
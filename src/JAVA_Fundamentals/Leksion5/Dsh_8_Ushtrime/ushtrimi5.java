package JAVA_Fundamentals.Leksion5.Dsh_8_Ushtrime;

/**
 * Shkruani nje program qe do te bej reverse nje string (perdorni loops dhe metodat e klases String)
 */
public class ushtrimi5 {
    public static void main(String[]args){
        String myText = "enri 1995";
        String text = "";
        for(int i = myText.length()-1; i >= 0; i--) {
            text =text + myText.charAt(i);
        }
        System.out.print("Reverse eshte: "+text);
    }
}
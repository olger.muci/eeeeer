package JAVA_Fundamentals.Leksion5.Dsh_8_Ushtrime;

import java.util.Scanner;

/**
 * shkruani nje program qe gjen gjatesine e nje stringu
 *
 */
public class ushtrimi3 {
    public static void main(String[]args){
        Scanner input=new Scanner(System.in);
        System.out.print("Shkruaj dicka ketu: ");
        String text= input.nextLine();
        System.out.println("Gjatesia eshte: "+text.length()); // do te numerohen dhe hapesirat midis shkronjave nese do kete
    }
}
//  Nje menyre tjeter do te ishte qe mund ta konsideronim si nje array me elemente te tipit char dhe mund ta benim me cikel
package JAVA_Fundamentals.Leksion5.Dsh_8_Ushtrime;

import java.util.Scanner;

/**
 * Shkruani nje program qe gjen karakterin ne nje index te dhene ne nje string.
 */

public class ushtrimi4 {
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);
        System.out.print("Shkruaj dicka: ");
        String text= input.nextLine();
        System.out.print("Vendos nje index: ");
        int index= input.nextInt();
        char karakter=text.charAt(index); // charAt(index) eshte metode qe jep karakterin per nje index te dhene
        System.out.print("Karakteri i kerkuar eshte: "+karakter); // numerohen dhe hapsirat mes fjaleve
    }
}
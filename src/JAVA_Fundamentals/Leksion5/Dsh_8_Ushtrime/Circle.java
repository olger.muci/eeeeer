package JAVA_Fundamentals.Leksion5.Dsh_8_Ushtrime;

/**
 * Krijoni nje klase Circle qe ka:
 * nje private instance variable e quajtur radius (data type double);
 * default constructor ku radius = 1.0;
 * nje constructor me nje parameter per radius;
 * dy metoda public per get dhe set te radius;
 * dy metoda private per gjetjen e siperfaqes dhe perimetrit te rrethit;
 * nje metode publike qe printon siperfaqen dhe perimetrin.
 */
public class Circle {
    private double radius;

    public Circle() {
        this.radius = 1.0D;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    private double calculateArea() {
        return Math.PI*radius*radius;
    }

    private double calculatePerimeter() {
        return 2*Math.PI*radius;
    }

    public void displayAreaAndPerimeter() {
        System.out.println("Siperfaqja eshte: " + this.calculateArea());
        System.out.println("Perimetri eshte: " + this.calculatePerimeter());
    }
}
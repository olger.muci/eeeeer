package JAVA_Fundamentals.Leksion5.Dsh_8_Ushtrime;

/**
 * ushtrimi 2
 * shkruani programin qe lexon nje array dhe gjen shumen e numrave tek dhe atyre cift.
 */
public class ushtrimi2 {
    public static void main(String[] args){
        int[] Array= new int[]{1,8,9,5,47,4,15,6,8,10,12,1};
        int shumaTek=0, shumaCift=0;
        for (int i=0; i<Array.length;i++) {
            if (Array[i]%2==1){
                shumaTek=shumaTek+Array[i];
            }
            else{
                shumaCift=shumaCift+Array[i];
            }
        }
        System.out.println("Shuma e numrave tek eshte: " +shumaTek+"\nShuma e numrave cift eshte: " +shumaCift);
    }
}
package JAVA_Fundamentals.Leksion5.Dsh_8_Ushtrime;

/**
 * Shkruani nje program ne java qe ben reverse nje array me vlera int.
 */

public class ushtrimi6 {
    public static void main(String[]args){
        int[] Array={1,3,0,8,1,9,9,5};
        int temp;           // marrim nje variabel qe do te ruaje vleren perkohesisht
        for(int i=0; i<Array.length/2; i++){    // ketu e bejme array.length/2 sepse pas vleres se mesit fillon serisht cikli te perseritet
            temp=Array[i];
            Array[i]=Array[Array.length-i-1];
            Array[Array.length-i-1]=temp;
        }
        System.out.print("Reverse eshte: ");
        for (int i=0; i<Array.length; i++){
            System.out.print(" "+ Array[i]+',');
        }
    }
}

/**
 * shihe me kujdes here pas here
 * jo dhe aq i lehte per tu realizuar
 */
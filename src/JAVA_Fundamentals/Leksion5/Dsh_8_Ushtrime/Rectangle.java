package JAVA_Fundamentals.Leksion5.Dsh_8_Ushtrime;

/**
 * Krijoni nje klase Rectangle:
 * me 2 private instance variables te quajtura width dhe length (data type float);
 * default constructor me vlere width = 1.0 dhe length = 1.0;
 * nje constructor me 2 parametra;
 * Shkruani getters dhe setters;
 * dhe dy metoda calculateArea & calculatePerimeter.
 */

public class Rectangle {
    private float width;
    private float length;

    public Rectangle() {
        this.width = 1.0f;
        this.length = 1.0f;
    }

    public Rectangle(float width, float length) {
        this.width = width;
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    private double calculateArea() {
        return this.width * this.length;
    }

    private double calculatePerimeter() {
        return 2*(this.width+this.length);
    }

//    public static void main(String[] args) {
//        Rectangle rectangle= new Rectangle();
//        rectangle.setLength(4);
//        rectangle.setWidth(8);
//
//        System.out.println("syprina eshte: "+rectangle.calculateArea());
//        System.out.println("perimetri eshte: "+rectangle.calculatePerimeter());
//    }
}
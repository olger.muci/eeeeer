package JAVA_Fundamentals.Leksion5.Dsh_8_Ushtrime;
/**
 * ushtrimi 1
 * Shkruani programin qe gjen faktorialin e nje numri.
 */

import java.util.Scanner;

public class faktoriali_i_nje_numri {
    public static void main(String[]args){
        System.out.print("Shkruani nje numer: ");
        Scanner input= new Scanner(System.in);
        int nr=input.nextInt();
        long factorial=1;
        for (int i=1;i<=nr;i++) {
            factorial =factorial* i;
        }
        System.out.println(nr+"!"+" = "+ factorial);
    }
}
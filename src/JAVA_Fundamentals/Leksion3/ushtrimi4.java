package JAVA_Fundamentals.Leksion3;

/**
 * Shkruani nje program qe do te gjej indeksin e nje elementi qe kerkohet ne nje array.
 */
public class ushtrimi4 {
    public static void main(String[]args){
        int[] array = {10,20,30,40,50,60,40,40};
        int element = 40;
        for (int i=0; i<array.length; i++) {
            if(array[i]==element) {
                System.out.println("Pozicioni eshte: "+i);
            }
        }
    }
}
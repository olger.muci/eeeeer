package JAVA_Fundamentals.Leksion3;

/**
 * Shkruani nje program ne java qe lexon te dhenat e nje array dhe gjen mesataren e tyre.
 */
public class ushtrimi3 {
    public static void main(String[]args){
        int[] array = {10, 20 , 30, 40};
        int shuma = 0, mesatare;
        for(int i=0;i<array.length;i++) {
            shuma=shuma+array[i];
        }
        mesatare=shuma/array.length;
        System.out.println("Mesatarja e kerkuar eshte: " + mesatare);
    }
}
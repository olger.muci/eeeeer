package JAVA_Fundamentals.Leksion3;

import java.util.Scanner;

/**
 *Shkruani nje program ne Java qe do te afishoj tabelen e shumezimit (perdorni loops)
 */
public class DSH3 {
    public static void main(String[]args){
        System.out.print("Vendos numrin: ");
        Scanner input =new Scanner(System.in);
        int a = input.nextInt();
        for(int i=1; i<=10;i++){
            System.out.println(a+" * "+i+" = "+a*i);
        }
    }
}
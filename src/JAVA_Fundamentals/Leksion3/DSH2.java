package JAVA_Fundamentals.Leksion3;

/**
 * Shkruani nje program ne Java qe do te afishoj shumen e 10 numrave te pare natyror (perdorni loops)
 */
public class DSH2 {
    public static void main(String[]args){
        int shuma=0;
        int i=1;
        while(i<=10){
            shuma=shuma+i;
            i++;
        }
        System.out.println("Shuma eshte "+shuma);
    }
}

/**
 *
 * menyra e dyte
 *
 *  int shuma=0;
 *        for (int i=1;i<=10;i++) {
 *            shuma = shuma + i;
 *        }
 *            System.out.println("Rezultati eshte: " +shuma);
 *
 */
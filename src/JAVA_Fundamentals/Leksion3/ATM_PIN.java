package JAVA_Fundamentals.Leksion3;

import java.util.Scanner;

public class ATM_PIN {
    public static void main( String[] args ){
        Scanner input = new Scanner(System.in);
        int pin = 12345;                                            // pini i sakte

        System.out.println("MIRESEERDHET NE BANKEN TONE! \n");
        System.out.print("VENDOSNI PININ TUAJ: ");
        int entry = input.nextInt();
        while ( entry != pin )
        {
            System.out.println("\n PIN-i JO I SAKTE. PROVO PERSERI.");
            System.out.print("VENDOSNI PININ TUAJ: ");
            entry = input.nextInt();
        }

        System.out.println("\nPIN-i ESHTE I SAKTE. MIRESEERDHET NE LLOGARINE TUAJ.");
    }
}
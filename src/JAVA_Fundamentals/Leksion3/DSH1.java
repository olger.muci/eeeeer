package JAVA_Fundamentals.Leksion3;

/**
 * Shkruani nje program ne Java qe do te afishoj numrat nga 1 deri ne 10;
 */
public class DSH1 {
    public static void main(String[] args) {
        System.out.print("Numrat jane:");
        for (int i = 1; i <= 10; i++) {
            System.out.print(" "+i+";");
        }
    }
}
package JAVA_Fundamentals.Leksion3;

/**
 *  Shkruani nje program ne Java qe lexon nje array dhe afishon elementet.
 */
public class ushtrimi1 {
    public static void main(String[]args){
        String[] array = new String[]{"A", "B", "C", "D", "E", "F", "G"};
        System.out.println("Elementet e array jane:");
        for (int i=0; i<array.length; i++) {
            System.out.print(" "+array[i]);
        }
    }
}
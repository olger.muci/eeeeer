package JAVA_Fundamentals.Leksion3;

import java.util.Scanner;

/**
 *  Ne nje ATM (bankomat) ju do vendosni PIN-in.
 *  Keni 3 mundesi maximale per te vendosur PIN-in.
 *  Nese nuk e gjeni, programi do nxjerr mesazhin "PIN-i jo i sakte, provoni perseri".
 *  Nese e gjeni, programi do nxjerr mesazhin "Mireserdhet ne llogari edhe balanca juaj eshte:<X>"
 */
public class ushtrimi5_ATM_PIN {
    public static void main(String[]args){
        int maxTries=3,pin=1995,pinCounter=0,enteredPin;
        Scanner input= new Scanner(System.in);
        System.out.println("MIRESEERDHET NE BANKEN TONE \n");
        while (pinCounter<maxTries){
            pinCounter++;
            System.out.print(pinCounter+". VENDOSNI PININ TUAJ: ");
            enteredPin= input.nextInt();
            if (enteredPin==pin){
                System.out.println("MIRESEERDHET NE LLOGARINE TUAJ");
                break;
            }
            else {
                System.out.println("PIN-i ESHTE I GABUAR.");
            }
        }
        if (pinCounter==maxTries){
            System.out.println("LLOGARIA JUAJ U BLLOKUA PER ARSYE SIGURIE.");
        }
    }
}
// shikoje rastin kur e vendosim pinin e sakte ne tentativen e trete
package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 *  Write an application calculating BMI (Body Mass Index) and checking if it's optimal or not.
 *  Your application should read two variables: weight (in kilograms, type float) and height (in centimeters, type int).
 *  BMI should be calculated given following formula: BMI = weight / height*height;
 *  The optimal BMI range is from 18.5 to 24.9, smaller or larger values are non-optimal values.
 *  Your program should write "BMI optimal" or "BMI not optimal", according to the assumptions above.
 */
public class Task2 {
    public static final float MIN_HEALTHY_BMI = 18.5F;  // e deklarojme jashte main, dmth eshte statike
    public static final float MAX_HEALTHY_BMI = 24.9F;  // e deklarojme jashte main, dmth eshte statike

    public static void main(String[] args) {
        float weight = enterWeight();
        int height = enterHeight();

        float bmi = calculateBMI(weight, height);
        System.out.println("BMI jote eshte: "+bmi);

        if(bmi >= MIN_HEALTHY_BMI && bmi <= MAX_HEALTHY_BMI) {
            System.out.print("BMI optimal");
        } else {
            System.out.print("BMI not optimal");
        }

    }

    public static float enterWeight() {
        System.out.print("Vendosni peshen tuaj ne kg: ");
        Scanner input = new Scanner(System.in);
        return input.nextFloat();
    }

    public static int enterHeight() {
        System.out.print("Vendosni gjatesine tuaj ne cm: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public static float calculateBMI(float weight, int height) {
        return (float) (weight/Math.pow((float)height / 100, 2));   // per llogaritjen e BMI mund ta benim dhe keshtu:
                                                                    // weight/(height/100)*(height/100) pjestim per 100 sepse e do ne m gjatesine
    }

}
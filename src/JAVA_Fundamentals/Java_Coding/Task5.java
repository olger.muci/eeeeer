package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 * Write an application that takes a positive number from the user (type int)
 * and prints all prime numbers greater than 1 and less than the given number.
 */
public class Task5 {
    public static void main(String[] args) {
        int value = enterNumber();
        printPrimes(value);
    }

    public static int enterNumber() {
        System.out.print("Vendos nje numer pozitiv: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public static boolean isPrime(int value) {
        for(int i = 2; i <= value/2; i++) {
            if(value % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void printPrimes(int value) {
        for(int i = 2; i < value; i++) {
            if(isPrime(i)) {
                System.out.println("Prime: " + i);
            }
        }
    }

}
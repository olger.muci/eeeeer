package JAVA_Fundamentals.Java_Coding;

import java.util.Scanner;

/**
 * Write an application that implements a simple calculator. The application should:
 * a) read first number (type float)
 * b) read one of following symbols: +; -; /; *
 * c) read second number (type float)
 * d) return a result of given mathematical operation
 */
public class Task8 {

    public static void main(String[] args) {
        int a = firstNumber();
        int b = secondNumber();
        char op= enterOperation();
        int rezultat= operation(op, a, b);
        System.out.print("Pergjigje: " + rezultat);

    }

    public static int firstNumber() {
        System.out.print("Vendos nr. e pare: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public static int secondNumber() {
        System.out.print("Vendosni nr. e dyte: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }
    public static char enterOperation(){
        System.out.print("Zgjidh veprimin: ");
        Scanner input= new Scanner(System.in);
        return input.next().charAt(0);

    }
    public static int operation(char op, int firstNumber, int secondNumber) {
        int result=0;

            if (op == '+') {
                result= firstNumber+secondNumber;
            }
            if (op== '-') {
                result= firstNumber-secondNumber;
            }
            if (op == '/') {
                result= firstNumber/secondNumber;
            }
            if (op == '*') {
                result= firstNumber*secondNumber;
            }
            return result;
    }
}
package JAVA_Fundamentals.Java_Coding;

import java.util.Scanner;

/**
 * Write an application that reads two lowercase letters of the Latin alphabet (type char)
 * and calculates how many characters is there in the alphabet between given letters.
 * Hint - use the ASCII code table and treat the characters as int numbers.
 */
public class Task14 {
    public static void main(String []args){
    Scanner input= new Scanner(System.in);
        System.out.print("Shkruaj germen e pare: ");
    char letter1= input.next().charAt(0);
        System.out.print("Shkruaj germen e dyte: ");
        char letter2= input.next().charAt(0);
        int difference= (letter2-letter1)-1;
        System.out.println(difference);
    }
}
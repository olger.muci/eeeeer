package JAVA_Fundamentals.Java_Coding;

import java.util.Scanner;

/**
 * Write an application that "stutters", that is, reads the user's text (type String), and prints
 * the given text, in which each word is printed twice.
 * For example, for the input: "This is my test" the application should print "This This is is
 * my my test test".
 */
public class Task13 {
    public static void main(String []args){
        System.out.print("shkrauj nje text: ");
        Scanner input= new Scanner(System.in);
        String [] array= input.nextLine().split(" "); //.split e ndan ne pjese, ne rastin tone aty ku eshte hapsira
    for (int i=0; i< array.length;i++){
        System.out.print(array[i]+" "+array[i]+"  ");
    }
    }
}
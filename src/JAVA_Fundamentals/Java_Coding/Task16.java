package JAVA_Fundamentals.Java_Coding;

/**
 * Write an application that takes 10 numbers from the user (type int) and write the length
 * of the longest such subsequence of these numbers, which is increasing.
 *
 * D.SH
 *
 */
public class Task16 {
    public static void main(String []args){
        int []array={1,3,8,4,2,5,6,11,13,7};
        int maxSequence=1; // sepse gjatesia minimumi do jete 1 pasi nje nr do e permbaje
        int sequence=1;
        for (int i=0;i< array.length-1;i++){
            if (array[i]<array[i+1]){
                sequence++;
            }
            if (maxSequence<sequence){
                maxSequence=sequence;
                sequence=1;
            }
        }
        System.out.println("Gjatesia e vargut me te gjate eshte: "+maxSequence);
    }
} // shihe prape se nuk del sic duhet rezultati
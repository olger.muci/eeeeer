package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 * Write an application that will take a positive number from the user (type int) and draw a
 * wave with a given length and height of 4 lines.
 */
public class Task9 {
    public static void main(String[] args) {
        int width = 4;
        int length = 4;
        Scanner input = new Scanner(System.in);
        System.out.print("Vendos numrin e perseritjes: ");
        int rep = input.nextInt();                              // rep eshte nr i perseritjeve
        int temp;

        for(int i = 0; i < length; i++) {
            temp = 0;
            do {
                for (int j = 0; j < width * 2; j++) {
                    if (i == j || i + j == width * 2 - 1) {     // ne rastin tone mund ta marrim dhe si i+j=7
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                temp++;
            } while (temp < rep);
            System.out.println();
        }
    }

}
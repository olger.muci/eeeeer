package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;


/**
 *   Write an application that will read diameter of a circle (type float) and calculate perimeter of given circle.
 *   Firstly, assume π = 3.14.
 *   Later, use value of π from built-in Math class.
 */
public class Task1 {
    public static final float PI = 3.14F;
    public static void main(String[] args) {
        float diameter = enterDiameter();
        float perimeter2 = calculatePerimeter(diameter);
        float perimeter = calculatePerimeterGivenPI(diameter, PI);

        System.out.println(" Perimetri me PI = 3.14 eshte: " + perimeter + " cm");
        System.out.print(" Perimetri eshte: " + perimeter2 + " cm."); // rezultati me 3 shifra pas presjes dhjetore
    }

    public static float enterDiameter() {
        System.out.print(" Vendos vleren e diametrit: ");
        Scanner sc = new Scanner(System.in);
        return sc.nextFloat();
    }

    public static float calculatePerimeter(float diameter) {
        return (float) Math.PI*diameter;
    }
    public static float calculatePerimeterGivenPI(float diameter, float pi) {
        return pi*diameter;
    }
}
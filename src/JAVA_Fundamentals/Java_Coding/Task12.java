package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 * Write an application that reads a text from the user (type String) and counts a percentage
 * of occurrences of a space character.
 */
public class Task12 {
    public static void main(String[] args) {
        String text = enterText();
        double percentage = calculateSpaceCharPercentage(text);

        System.out.println("Perqindja e char space eshte: " + percentage + " %");
    }

    public static String enterText() {
        System.out.print("Shkruaj nje text: ");
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }

    public static double calculateSpaceCharPercentage(String text) {
        int frequency = 0;
        for(int i = 0; i < text.length(); i++) {
            if(text.charAt(i) == ' ') {
                frequency++;
            }
        }
        return ((double)frequency/text.length())*100;
    }

}
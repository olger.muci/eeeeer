package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 * Write a program for solving a quadratic equation.
 *   The program should take three integers (coefficients of the quadratic equation a, b, c) and calculate the roots equation.
 *   If delta ∆ comes out negative, print "Delta negative" and exit the program.
 *   Formulas you'll need: D = b*b - 4*a*c; x1 = (-b - sqrt(D)) / 2a; x1 = (-b + sqrt(D)) / 2a;
 */
public class Task3 {
    public static void main(String[] args) {
        int a = enterQuadraticCoefficient();
        int b = enterQuadraticCoefficient();
        int c = enterQuadraticCoefficient();

        double delta = calculateDelta(a, b, c);

        if(delta > 0) {
            double x1 = calculateFirstRoot(a, b, delta);
            double x2 = calculateSecondRoot(a, b, delta);
            System.out.println("Ekuacioni ka dy zgjidhje: " +x1+ " &  "+x2);
        } else if (delta==0){
            double x1=calculateFirstRoot(a,b,delta);
            double x2=calculateSecondRoot(a,b,delta);
            System.out.println("Ekuacioni ka rrenje te dyfishte: "+x1);
        }
        else {
            System.out.println("Dallori eshte negativ. Ekuacioni i dhene nuk ka zgjidhje ne R.");
        }
    }

    public static int enterQuadraticCoefficient() {
        System.out.print("Vendos koeficientin: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public static double calculateDelta(int a, int b, int c) {
        return Math.pow(b, 2) - (4*a*c);       // mund ta benim dhe b*b-4*a*c
    }

    public static double calculateFirstRoot(int a, int b, double delta) {
        return (-b - Math.pow(delta, 0.5))/(2*a);   // mund te behej dhe (-b-sqrt(delta))/2*a
    }

    public static double calculateSecondRoot(int a, int b, double delta) {
        return (-b + Math.pow(delta, 0.5))/(2*a);      // mund te behej dhe (-b+sqrt(delta))/2*a
    }

}
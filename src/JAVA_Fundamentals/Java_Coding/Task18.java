package JAVA_Fundamentals.Java_Coding;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Write an application that reads a text from the user (type String) and checks whether the
 * user sneezed, i.e. whether the text equals Ͽachooo!Ѐ with one or more letter "o" at the end
 * of the expression (so both 'acho!' and 'achooooooo! are correct).
 */
public class Task18 {
    public static void main(String []args) {

    }
    public static String enterText(){
        System.out.println("Shkruaj nje text: ");
        return enterText();
    }

    public static boolean hasUserSneezd(String text){
        String regex= "acho+!";
        Pattern pattern= Pattern.compile(regex);
        Matcher matcher= pattern.matcher(text);
        return matcher.matches();
    }
}
package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 * Write an application that gets one positive number (type int) from the user and calculates
 * a sum of digits of the given number.
 */
public class Task10 {
    public static void main(String[] args) {
        int number = enterNumber();
        System.out.println(calculateDigits(number));
    }

    public static int enterNumber() {
        System.out.print("Vendos nje numer: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public static int calculateDigits(int number) {
        int sum=0;
        while(number != 0) {
            sum=sum+number%10;      //  shumes i shtohet mbetja qe merret nga pjestimi i nr me 10, dmth shifra e fundit
            number=number/10;       //  numri do t hiqet shifra e fundit duke e pjesetuar ate me 10 dhe qe ketu rinis cikli perseri
        }
        return sum;
    }

}
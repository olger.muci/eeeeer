package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 * Write an application that will read texts (variables of the String type) until the user gives
 * the text "Enough!" and then writes the longest of the given texts (not including the text
 * "Enough!"). If the user does not provide any text, write "No text provided".
 */
public class Task11 {
    public static void main(String[] args) {
        String longestText = findLongestText();
        printLongestText(longestText);
    }

    public static String findLongestText() {
        Scanner input = new Scanner(System.in);

        String text = "";
        String longestText = "";

        do {
            if(text.length() > longestText.length()) {
                longestText = text;
            }
            System.out.println("Shkruaj");
            text = input.nextLine();;
        } while(!text.equals("Enough!"));

        return longestText;
    }

    public static void printLongestText(String longestText) {
        if(longestText.length() == 0) {
            System.out.println("No text provided");
        } else {
            System.out.println("Longest text was: " + longestText);
        }
    }

}
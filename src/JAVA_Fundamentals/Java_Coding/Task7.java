package JAVA_Fundamentals.Java_Coding;

import java.util.Scanner;

/**
 * Write an application that will take a positive number from the user (type int) and
 * calculate the Fibonacci number at the indicated index.
 */
public class Task7 {
    public static void main(String[] args) {                //Vargu i Fibonaccit: 0,1,1,2,3,5,8,13,21,34,55...
        System.out.println("Nr. i Fibonaccit per indexin e kerkuar eshte: "+fibonacci(index()));
    }
    public static int index() {
        System.out.print("Vendos nje index: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }
    public static long fibonacci(int n){
        if (n<=1){
            return n;
        } else {
            return (fibonacci(n-1)+fibonacci(n-2));
        }
    }
}
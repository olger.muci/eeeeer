package JAVA_Fundamentals.Java_Coding;

import java.util.Random;
import java.util.Scanner;

/**
 * Write an application that will play "too much, too little" game with you. At the beginning
 * the application should randomly choose a number from 0 to 100.
 * (hint: use the Random.nextInt() method) and wait for the user to enter a number.
 * If the user gives a number greater than the number chosen by the computer, your application should print
 * "too much" and wait for the next number.
 * If the user gives a smaller number, the application should print "not enough" and wait for the next number in the same way.
 * If the user provides the exact value, the application should print the word "Congratulations!" and finish.
 */
public class Task20 {
    public static void main(String []args){
        int randomNumber= new Random().nextInt(101); // vendosim kufirin e siperm 101 sepse duam te afishojme numrat deri ne 0-100
        int enteredNumber;
        do {
            System.out.print("vendosni nr tuaj: ");
            Scanner input= new Scanner(System.in);
            enteredNumber= input.nextInt();

            if (enteredNumber<randomNumber){
                System.out.println("To little!");
            }
            else if (enteredNumber>randomNumber) {
                System.out.println("Too much!");
            }
            else {
                System.out.println("CONGRATS!!!");
            }
        }
        while (enteredNumber!=randomNumber);
    }
}
package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 * Write an application that takes a positive number from the user (type int) and writes all numbers from 1 to the given number,
 *      each on the next line, with the following changes:
 *   - in place of numbers divisible by 3, instead of a number the program should print "Fizz";
 *   - in place of numbers divisible by 7, instead of a number the program should write "Buzz";
 *   - if the number is divisible by both 3 and 7, the program should print "Fizz buzz"
 */
public class Task4 {
    public static void main(String[] args) {
        int value = enterNumber();

        checkNumbers(value);
    }

    public static int enterNumber() {
        System.out.print("Vendos nje numer: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public static boolean isDivisibleBy3(int value) {
        return value % 3 == 0;
    }

    public static boolean isDivisibleBy7(int value) {
        return value % 7 == 0;
    }

    public static void checkNumbers(int value) {
        for(int i = 1; i <= value; i++) {
            if(isDivisibleBy3(i) && isDivisibleBy7(i)) {       // kete kushtin e checkojme te parin pasi ploteson me shume kushte njekohesiht
                System.out.println("Fizz buzz");
            } else if(isDivisibleBy3(i)) {
                System.out.println("Fizz");
            } else if(isDivisibleBy7(i)) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

}
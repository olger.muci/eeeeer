package JAVA_Fundamentals.Java_Coding;
import java.util.Scanner;
/**
 * Write an application that takes a number n from the user (type int)
 *   and calculates the sum of the harmonic series from 1 to n,
 *   according to the formula below: 1 + 1/2 + 1/3 + ... + 1/n
 */
public class Task6 {
    public static void main(String[] args) {
        int maxValue = enterNumber();
        double harmonicSeriesSum = calculateHarmonicSeries(maxValue);

        System.out.printf("Shuma eshte: %.2f", harmonicSeriesSum);
    }

    public static int enterNumber() {
        System.out.print("Vendos nje numer: ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public static double calculateHarmonicSeries(int maxValue) {
        double sum = 0F;
        for(int i = 1; i <= maxValue; i++) {
            sum = sum + 1/(double)i;
        }

        return sum;
    }

}
package JAVA_Fundamentals.Leksion7.App_per_librarine_online;

public class BookListBuilder {
    private static Book[] books;

    public static Book[] buildBooks(){
        books = new Book[100];

        books[0] = new Book();
        books[0].setId(1);
        books[0].setTitle("Libri 1");
        books[0].setAuthor("Autori 1");
        books[0].setPageNumbers(100);
        books[0].setQuantity(0);

        books[1] = new Book();
        books[1].setId(2);
        books[1].setTitle("Libri 2");
        books[1].setAuthor("Autori 2");
        books[1].setPageNumbers(200);
        books[1].setQuantity(1);

        books[2] = new Book();
        books[2].setId(3);
        books[2].setTitle("Libri 3");
        books[2].setAuthor("Autori 3");
        books[2].setPageNumbers(300);
        books[2].setQuantity(2);

        books[3] = new Book();
        books[3].setId(4);
        books[3].setTitle("Libri 4");
        books[3].setAuthor("Autori 4");
        books[3].setPageNumbers(400);
        books[3].setQuantity(3);

        books[4] = new Book();
        books[4].setId(5);
        books[4].setTitle("Libri 5");
        books[4].setAuthor("Autori 1");
        books[4].setPageNumbers(200);
        books[4].setQuantity(40);

        books[5] = new Book();
        books[5].setId(6);
        books[5].setTitle("Libri 6");
        books[5].setAuthor("Autori 1");
        books[5].setPageNumbers(200);
        books[5].setQuantity(50);

        books[6] = new Book();
        books[6].setId(7);
        books[6].setTitle("Libri 7");
        books[6].setAuthor("Autori 2");
        books[6].setPageNumbers(500);
        books[6].setQuantity(60);

        books[7] = new Book();
        books[7].setId(8);
        books[7].setTitle("Libri 8");
        books[7].setAuthor("Autori 2");
        books[7].setPageNumbers(100);
        books[7].setQuantity(70);

        books[8] = new Book();
        books[8].setId(9);
        books[8].setTitle("Libri 9");
        books[8].setAuthor("Autori 1");
        books[8].setPageNumbers(100);
        books[8].setQuantity(80);

        books[9] = new Book();
        books[9].setId(10);
        books[9].setTitle("Libri 10");
        books[9].setAuthor("Autori 3");
        books[9].setPageNumbers(100);
        books[9].setQuantity(90);

        return books;
    }
}
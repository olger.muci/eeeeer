package JAVA_Fundamentals.Leksion7.App_per_librarine_online;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.regex.Pattern;


public class Main {
    private static final Book[] books = BookListBuilder.buildBooks();
    private static final User[] users = UserListBuilder.buildUsers();
    private static User currentUser = new User();

    public static void main(String[] args) {
        displayWelcomeMenu();
        listAllUsers();
    }

    public static void listAllUsers() {
        for(User user : users) {
            if(user != null) {
                System.out.println(user);
            }
        }
    }

    public static void displayWelcomeMenu() {
        System.out.println("===============================");
        System.out.println(" Welcome to the Library. ");
        System.out.println(" 1. Log in to your account. ");
        System.out.println(" 2. Sign up. ");
        System.out.println(" 3. Exit. ");
        System.out.print(" Choose an option: ");
        Scanner input = new Scanner(System.in);
        redirectWelcomeMenuSelection(input.nextInt());
    }

    public static void displayUserMenu(User user) {
        System.out.println(" Welcome: " + user.getFirstName() + " " + user.getLastName());
        System.out.println(" 1. Find a book. ");
        System.out.println(" 2. List all books. ");
        System.out.println(" 3. View my rented books. ");
        System.out.println(" 4. Log out. ");

        System.out.print(" Choose an option: ");
        Scanner input = new Scanner(System.in);
        redirectUserMenuSelection(input.nextInt());
    }

    public static void displayFindBookMenu() {
        System.out.println("===============================");
        System.out.println(" 1. Search by title. ");
        System.out.println(" 2. Search by author. ");

        System.out.println();
        System.out.println(" Choose an option: ");
        Scanner input = new Scanner(System.in);
        redirectFindBookMenuSelection(input.nextInt());
    }

    public static void redirectFindBookMenuSelection(int selection) {
        switch (selection) {
            case 1:
                displayBookTitleSearch();
                break;
            case 2:
                displayBookAuthorSearch();
                break;
        }
    }

    public static void displayBookTitleSearch() {
        System.out.println("===============================");
        System.out.print("Enter title: ");
        Scanner input = new Scanner(System.in);
        String titleSearch = input.nextLine();

        Book[] booksResult = new Book[books.length];
        int j = 0;
        for(Book book : books) {
            if(book != null) {
                if(book.getTitle().contains(titleSearch)) {
                    booksResult[j] = book;
                    j++;
                }
            }
        }

        for(Book book : booksResult) {
            if(book != null) {
                System.out.println(book.getId() + ". " + book);
            }
        }

        if(booksResult[0] != null) {
            displayBookMenu();
        } else {
            displayFindBookMenu();
        }
    }

    public static void displayBookAuthorSearch() {
        System.out.print("Enter author: ");
        Scanner input = new Scanner(System.in);
        String authorSearch = input.nextLine();

        Book[] booksResult = new Book[books.length];
        int j = 0;
        for(Book book : books) {
            if(book != null) {
                if(book.getAuthor().contains(authorSearch)) {
                    booksResult[j] = book;
                    j++;
                }
            }
        }

        for(Book book : booksResult) {
            if(book != null) {
                System.out.println(book);
            }
        }

        if(booksResult[0] != null) {
            displayBookMenu();
        } else {
            displayFindBookMenu();
        }
    }
    public static void redirectUserMenuSelection(int selection) {
        switch(selection) {
            case 1:
                displayFindBookMenu();
                break;
            case 2:
                displayAllBooksMenu();
                break;
            case 3:
                displayRentedBooksMenu();
                break;
            case 4:
                currentUser = null;
                displayWelcomeMenu();
                break;
        }
    }

    public static void displayRentedBooksMenu() {
        System.out.println("===============================");
        Book[] rentedBooks = new Book[100];
        int i = 0;
        for(Book book : books) {
            if(book != null) {
                for(int id : currentUser.getRentedBooks()) {
                    if(id == book.getId()) {
                        rentedBooks[i] = book;
                        i++;
                    }
                }
            }
        }

        for(Book book : rentedBooks) {
            if(book != null) {
                System.out.println((book.getId()) + ". " + book);
            }
        }

        System.out.println();
        System.out.println("1. Choose a book: ");
        System.out.println("2. Go back: ");
        Scanner input = new Scanner(System.in);
        redirectAllBooksMenu(input.nextInt());
    }

    public static void displayAllBooksMenu() {
        System.out.println("===============================");
        for(int i = 0; i < books.length; i++) {
            if(books[i] != null) {
                System.out.println((books[i].getId()) + ". " + books[i]);
            }
        }
        System.out.println();
        System.out.println("1. Choose a book: ");
        System.out.println("2. Go back: ");
        Scanner input = new Scanner(System.in);
        redirectAllBooksMenu(input.nextInt());

    }

    public static void redirectAllBooksMenu(int selection) {
        switch (selection) {
            case 1:
                displayBookMenu();
                break;
            case 2:
                displayUserMenu(currentUser);

        }
    }

    public static void displayBookMenu() {
        System.out.println();
        System.out.print("Choose a book: ");
        Scanner input = new Scanner(System.in);
        redirectBookMenu(input.nextInt());
    }

    public static void redirectBookMenu(int bookId) {
        displayBookDetailsMenu(bookId);
    }

    public static void displayBookDetailsMenu(int bookId) {
        System.out.println("===============================");
        System.out.println("Book details: ");
        Book currentBook = new Book();

        for(Book book : books) {
            if(book.getId() == bookId) {
                currentBook = book;
                break;
            }
        }

        System.out.println("Title: " + currentBook.getTitle());
        System.out.println("Author: " + currentBook.getAuthor());
        System.out.println("Stock: " + currentBook.getQuantity());
        System.out.println();
        System.out.println("1. Rent");
        System.out.println("2. Return");
        System.out.println("3. Go back");
        Scanner input = new Scanner(System.in);
        redirectBookDetailsMenu(input.nextInt(), currentBook);

    }

    public static void redirectBookDetailsMenu(int selection, Book currentBook) {
        switch (selection) {
            case 1:
                displayRentBookMenu(currentBook);
                break;
            case 2:
                displayReturnBookMenu(currentBook);
                break;
            case 3:
                displayUserMenu(currentUser);
                break;
        }
    }

    public static void displayReturnBookMenu(Book currentBook) {
        System.out.print("Confirm? (Y/N) ");
        Scanner input = new Scanner(System.in);

        int[] rentedBooks = new int[100];
        int i = 0;
        for(Book book : books) {
            if(book != null) {
                for(int bookId : currentUser.getRentedBooks()){
                    if(bookId == book.getId()) {
                        rentedBooks[i] = book.getId();
                        i++;
                    }
                }
            }
        }

        for(int j = 0; j < rentedBooks.length; j++) {
            if(rentedBooks[j] == currentBook.getId()) {
                rentedBooks[j] = 0;
                currentUser.setRentedBooks(rentedBooks);
                break;
            }
        }

        if(input.next().equals("Y")) {
            if(isQuantityAvailable(currentBook)) {
                increaseBookQuantity(currentBook);
            }
        }

        displayUserMenu(currentUser);
    }

    public static boolean isQuantityAvailable(Book currentBook) {
        for(Book book : books) {
            if(book != null) {
                if(book.getId() == currentBook.getId()) {
                    return book.getQuantity() > 0;
                }
            }
        }
        return false;
    }

    public static void reduceBookQuantity(Book currentBook) {
        for(Book book : books) {
            if(book != null) {
                if(book.getId() == currentBook.getId()) {
                    book.setQuantity(book.getQuantity()-1);
                }
            }
        }
    }

    public static void increaseBookQuantity(Book currentBook) {
        for(Book book : books) {
            if(book != null) {
                if(book.getId() == currentBook.getId()) {
                    book.setQuantity(book.getQuantity()+1);
                }
            }
        }
    }

    private static void displayRentBookMenu(Book currentBook) {
        System.out.print("Confirm? (Y/N) ");
        Scanner input = new Scanner(System.in);

        int[] rentedBooks = new int[100];
        int i = 0;
        for(Book book : books) {
            if(book != null) {
                for(int bookId : currentUser.getRentedBooks()){
                    if(bookId == book.getId()) {
                        rentedBooks[i] = book.getId();
                        i++;
                    }
                }
            }
        }

        if(input.next().equals("Y")) {
            if(isQuantityAvailable(currentBook)) {
                rentedBooks[i] = currentBook.getId();
                reduceBookQuantity(currentBook);
                currentUser.setRentedBooks(rentedBooks);
            }
        }

        displayUserMenu(currentUser);
    }

    public static void redirectWelcomeMenuSelection(int selection) {
        switch(selection) {
            case 1:
                logIn();
                break;
            case 2:
                signUp();
                break;
            case 3:
                System.out.println("Exiting...");
                break;
        }
    }

    public static void signUp() {
        final int max_tries = 3;
        Scanner input = new Scanner(System.in);
        System.out.println("===============================");
        System.out.println("Welcome to sign up");
        System.out.print("Enter first name: ");
        String firstName = input.next();

        System.out.print("Enter last name: ");
        String lastName = input.next();

        System.out.print("Enter date of birth (dd.MM.yyyy): ");
        String dob = input.next();

        int current_tries = 0;
        String username;
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]+$");
        do {
            System.out.println("Enter username (only letters and numbers): ");
            username = input.next();

            if(pattern.matcher(username).matches()) {
                break;
            }

            System.out.println("Username is incorrect");
            current_tries++;

        } while(max_tries > current_tries);

        current_tries = 0;

        pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$");
        String password;
        do {
            System.out.println("Enter password (At least 1 lower and uppercase, at least one number): ");
            password = input.next();

            if(pattern.matcher(password).matches()) {
                break;
            }

            System.out.println("Password is incorrect");
            current_tries++;

        } while(max_tries > current_tries);

        addUser(firstName, lastName, dob, username, password);
    }

    public static void addUser(String firstName, String lastName, String dob, String username, String password) {
        for(int i = 0; i < users.length; i++) {
            if(users[i] == null) {
                users[i] = new User(firstName, lastName, LocalDate.parse(dob, DateTimeFormatter.ofPattern("dd.MM.yyyy")), username, password);
                break;
            }
        }
    }

    public static void logIn() {
        Scanner input = new Scanner(System.in);
        System.out.println("===============================");
        System.out.print("Hello, please enter your username: ");
        String username = input.nextLine();
        System.out.print("Please enter your password: ");
        String password = input.nextLine();
        for(User user : users) {
            if(user != null) {
                if(user.getUsername().equals(username)) {
                    if(user.getPassword().equals(password)){
                        System.out.println("===============================");
                        System.out.println(" Logged in successfully! ");
                        currentUser = user;
                        displayUserMenu(currentUser);
                    }
                }
            }
        }
    }

}
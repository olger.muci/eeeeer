package JAVA_Fundamentals.Leksion7.App_per_librarine_online;
import java.time.LocalDate;
public class UserListBuilder {
    public static User[] users;

    public static User[] buildUsers() {
        users = new User[100];

        users[0] = new User();
        users[0].setFirstName("Enri");
        users[0].setLastName("Skenderaj");
        users[0].setDateOfBirth(LocalDate.of(1995,8,13));
        users[0].setUsername("enriskenderaj");
        users[0].setPassword("enri0611");
        users[0].setRentedBooks(new int[]{3,4});

        users[1] = new User();
        users[1].setFirstName("Jane");
        users[1].setLastName("Doe");
        users[1].setDateOfBirth(LocalDate.of(2000,01,01));
        users[1].setUsername("janedoe");
        users[1].setPassword("jane2000");
        users[1].setRentedBooks(new int[]{7});

        users[2] = new User();
        users[2].setFirstName("Joe");
        users[2].setLastName("Doe");
        users[2].setDateOfBirth(LocalDate.of(1995,01,01));
        users[2].setUsername("joedoe");
        users[2].setPassword("joe2000");

        return users;
    }
}
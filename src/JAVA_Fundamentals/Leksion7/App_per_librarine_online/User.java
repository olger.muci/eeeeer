package JAVA_Fundamentals.Leksion7.App_per_librarine_online;
import java.time.LocalDate;
import java.util.Arrays;

public class User {
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String username;
    private String password;
    private int[] rentedBooks;

    public User() {
    }

    public User(String firstName, String lastName, LocalDate dateOfBirth, String username, String password, int[] rentedBooks)
            {
            this.firstName = firstName;
            this.lastName = lastName;
            this.dateOfBirth = dateOfBirth;
            this.username = username;
            this.password = password;
            this.rentedBooks = rentedBooks;
    }

    public User(String firstName, String lastName, LocalDate dateOfBirth, String username, String password)
        {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.username = username;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int[] getRentedBooks() {
        return rentedBooks;
    }

    public void setRentedBooks(int[] rentedBooks) {
        this.rentedBooks = rentedBooks;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", rentedBooks=" + Arrays.toString(rentedBooks) +
                '}';
    }

}
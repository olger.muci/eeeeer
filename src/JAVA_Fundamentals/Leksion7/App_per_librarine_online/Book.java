package JAVA_Fundamentals.Leksion7.App_per_librarine_online;

public class Book {
    private int id;
    private String title;
    private String author;
    private int pageNumbers;
    private int quantity;

    public Book() {
    }

    public Book(int id, String title, String author, int pageNumbers, int quantity) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.pageNumbers = pageNumbers;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPageNumbers() {
        return pageNumbers;
    }

    public void setPageNumbers(int pageNumbers) {
        this.pageNumbers = pageNumbers;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", pageNumbers=" + pageNumbers +
                ", quantity=" + quantity +
                '}';
    }
}
package JAVA_Fundamentals.Leksion7.Ushtrime;

import java.util.Arrays;

/**
 * Te shkruhet programi ne java qe rendit nje array me vlera int ne rend rrites
 */
public class Ushtrimi2_Menyra_2 {
    public static void main(String[] args) {
    int [] Array={2,5,7,1,10,12,15,1,3,19};
    Arrays.sort(Array);             // kjo eshte komande e gatshme qe perdoret per renditje ne rend rrites
        System.out.println("Array i renditur eshte: "+ Arrays.toString(Array));
    }
}
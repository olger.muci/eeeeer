package JAVA_Fundamentals.Leksion7.Ushtrime;
import java.util.Scanner;

public class Ushtrimi1_Menyra_2 {
    public static void main(String[] args) {
        String text = "";
        Scanner input= new Scanner(System.in);
        System.out.print("Shkruaj nje fjale: ");
        text= input.next();
        String text2="";
        boolean isPalindrome= true;                 // e quajme flag (tip boolean), dmth permban vlere true ose false
        for (int i=0;i<text.length()/2;i++){
            if (text.charAt(i)!=text.charAt(text.length()-1-i)){
            isPalindrome= false;
            break;
            }
        }
        System.out.println(isPalindrome);
    }
}
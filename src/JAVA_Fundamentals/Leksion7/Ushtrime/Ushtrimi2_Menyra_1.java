package JAVA_Fundamentals.Leksion7.Ushtrime;

/**
 * krijoni nje program ne java qe rendit nje array me vlera int, ne rend rrites.
 *
 */
public class Ushtrimi2_Menyra_1 {
    public static void main(String[] args) {
        int[] Array={2,5,3,2,6,9,4};
        int temp;
        System.out.print("Array i renditur eshte:");
        for (int i=0; i<Array.length;i++){
            for (int j=i+1;j<Array.length;j++){
                if(Array[i]>Array[j]){
                temp = Array[i];
                Array[i]=Array[j];
                Array[j]=temp;
                }
            }
            System.out.print(" "+Array[i]+",");
       }
    }
}
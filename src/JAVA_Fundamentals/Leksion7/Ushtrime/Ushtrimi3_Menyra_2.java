package JAVA_Fundamentals.Leksion7.Ushtrime;

public class Ushtrimi3_Menyra_2 {
    public static void main(String[] args) {
        int[] array={1, 2, 8, 3, 2, 2, 2, 5, 1};
        int[] frequence = new int[array.length];        // do te ruaj frekuencat e elementeve
        int visited = -1;
        for(int i=0; i<array.length; i++){
            int count=1;
            for(int j=i+1; j<array.length; j++){
                if(array[i]==array[j]){
                    count++;
                    frequence[j] = visited;     // e bejme qe te shmangim numerimin e elementeve te njejte
                }
            }
            if(frequence[i]!=visited)
                frequence[i]=count;
        }
        System.out.println("---------------------------------------");
        System.out.println("Elementi | Frekuenca");
        System.out.println("---------------------------------------");
        for(int i=0; i<frequence.length; i++){
            if(frequence[i]!=visited)
                System.out.println("    " + array[i] + "    |    " + frequence[i]);
        }
        System.out.println("----------------------------------------");
    }
}
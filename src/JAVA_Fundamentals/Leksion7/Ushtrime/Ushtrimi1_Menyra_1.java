package JAVA_Fundamentals.Leksion7.Ushtrime;

import java.util.Scanner;

/**
 *
 * Krijoni nje program ne java qe checkon nese nje string eshte palindrome.
 *
 * palindrome jane fjalet qe lexohen njelloj nga te dyja anet, psh. ANA; IVI; ARRA; etj.
 * Mund te kete dhe fjale pa kuptim qe jane palindroma.
 *
 */
public class Ushtrimi1_Menyra_1 {
    public static void main(String[]args){
        String text = "";
        Scanner input= new Scanner(System.in);
        System.out.print("Shkruaj nje fjale: ");
        text= input.next();
        String text2="";
        for (int i=text.length()-1;i>=0;i--){
            text2=text2+text.charAt(i);
        }
        System.out.println(text.equals(text2));
    }
}
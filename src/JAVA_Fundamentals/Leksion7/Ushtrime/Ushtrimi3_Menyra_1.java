package JAVA_Fundamentals.Leksion7.Ushtrime;

import java.util.Scanner;

/**
 * krijoni nje program ne java qe gjen elementin me frekuencen me te madhe ne nje array (int).
 */
public class Ushtrimi3_Menyra_1 {
    public static void main(String[] args) {
        System.out.println("Shkruani numrat tuaj: ");
        Scanner input = new Scanner(System.in);
        int[] array = new int[5];
        int freq=1;
        int maxFreq=1;
        int maxFreqElement=array[0];
        for (int k = 0; k < array.length; k++) {
            array[k] = input.nextInt();
        }
        for (int i = 0; i < array.length; i++) {
            freq = 1;                     // kjo behet qe per cdo element qe do fillojme te kontrollojme, frekuenca te filloj nga 1.
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    freq++;
                }
                if (freq > maxFreq) {
                    maxFreq = freq;
                    maxFreqElement = array[i];
                }
            }
        }
        System.out.println("Elementi me frekuence me te madhe eshte: " + maxFreqElement);
    }
}
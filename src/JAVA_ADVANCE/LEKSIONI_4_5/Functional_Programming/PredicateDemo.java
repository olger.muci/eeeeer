package JAVA_ADVANCE.LEKSIONI_4_5.Functional_Programming;

import java.util.function.Predicate;

public class PredicateDemo {
    public static void main(String[] args) {
        String email1 = "abc@gmail.com";
        String email2 = "abc@gmail.ca";

        System.out.println(isEmailValid(email1));
        System.out.println(isEmailValid(email2));

        Predicate<String> isEmailValid = email -> email.endsWith(".com");

        System.out.println(isEmailValid.test(email1));
        System.out.println(isEmailValid.test(email2));
    }

    static boolean isEmailValid(String email) {
        return email.endsWith(".com");
    }
}
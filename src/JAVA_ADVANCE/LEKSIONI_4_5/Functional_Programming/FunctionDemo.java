package JAVA_ADVANCE.LEKSIONI_4_5.Functional_Programming;
import java.util.function.Function;

public class FunctionDemo {
    public static void main(String[] args) {
        int increment = increment(10);
        int multiply = multiplyByTen(increment);

        System.out.println(multiply);

        Function<Integer, Integer> incrementByOne = number -> number + 1;

        Function<Integer, Integer> multiplyByTen = number -> number * 10;

        Function<Integer, Integer> incrementAndMultiplyByTen =
                number ->
                        incrementByOne
                                .andThen(multiplyByTen)
                                .apply(number);

        System.out.println(incrementAndMultiplyByTen.apply(10));
    }

    static int increment(int number) {
        return number + 1;
    }

    static int multiplyByTen(int number) {
        return number * 10;
    }
}
package JAVA_ADVANCE.LEKSIONI_4_5.Functional_Programming;

import JAVA_ADVANCE.LEKSIONI_4_5.FILE_ION_IO.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Demo {
    public static void main(String[] args) {
        User user1 = new User(1, "john", "abc123", "Gerard", "Rama", LocalDate.of(1980,1,1));
        User user2 = new User(2, "alex", "def456", "Alex", "Doe", LocalDate.of(1985,1,1));
        User user3 = new User(3, "jane", "ghi789", "Jane", "Doe", LocalDate.of(1990,1,1));

        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);

        List<User> filteredUser = new ArrayList<>();
        List<Integer> filteredIds = new ArrayList<>();
//        for(User user : users) {
//            if(user.getId() > 1) {
//                filteredUser.add(user);
//            }
//        }

        filteredIds = users
                .stream()
                .filter(user -> user.getId() > 1)
                .filter(user -> user.getFirstName().equals("Jane"))
                .map(User::getId)
                .collect(Collectors.toList());

        System.out.println(filteredIds);















//        Collections.sort(users, new Comparator<User>() {
//            @Override
//            public int compare(User o1, User o2) {
//                return o1.getId().compareTo(o2.getId());
//            }
//        });
//
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("Hello");
//            }
//        };

//        Runnable runnable1 = () -> System.out.println("Hello");
//
//        DemoInterface demo = new DemoInterface() {
//            @Override
//            public void show(String text) {
//                if(text.equals("This is a test")) {
//                    System.out.println(text);
//                } else {
//                    System.out.println("I dont know");
//                }
//            }
//        };
//
//        DemoInterface JAVA_ADVANCE.LEKSIONI_4_5.Threads.demo1.demo1 = test -> {
//            if(test.equals("This is a test")) {
//                System.out.println(test);
//            } else {
//                System.out.println("I dont know");
//            }
//        };
//
//        demo.show("This is a test");
//
//        JAVA_ADVANCE.LEKSIONI_4_5.Threads.demo1.demo1.show("This is test2");
    }
}
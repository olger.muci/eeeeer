package JAVA_ADVANCE.LEKSIONI_4_5.Functional_Programming;
import java.util.function.Consumer;


public class ConsumerDemo {
    public static void main(String[] args) {
        Customer customer = new Customer("Mia", "0681234567");
//        greetCustomer(customer);

        Consumer<Customer> greetCustomer =
                customer1 ->
                        System.out.println("Hello " + customer.name + ", thanks for registering phone number: " + customer.phone);

        greetCustomer.accept(customer);
    }

    static void greetCustomer(Customer customer) {
        System.out.println("Hello " + customer.name + ", thanks for registering phone number: " + customer.phone);
    }

    static class Customer {
        private final String name;
        private final String phone;

        Customer(String name, String phone) {
            this.name = name;
            this.phone = phone;
        }
    }

}
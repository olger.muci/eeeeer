package JAVA_ADVANCE.LEKSIONI_4_5.Functional_Programming;

import java.util.function.Supplier;

public class SupplierDemo {
    public static void main(String[] args) {
        String lecture = getLecture();
        System.out.println(lecture);

        Supplier<String> getLecture = () -> "JavaTiranaAL25";
        System.out.println(getLecture.get());
    }

    static String getLecture() {
        return "JavaTiranaAL25";
    }

}
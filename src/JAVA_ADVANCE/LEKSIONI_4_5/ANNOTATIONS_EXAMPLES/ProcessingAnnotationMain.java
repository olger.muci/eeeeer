package JAVA_ADVANCE.LEKSIONI_4_5.ANNOTATIONS_EXAMPLES;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

// public class ProcessingAnnotationMain {
//    public static void main(String[] args) throws Exception{
//        Cat cat = new Cat("Sylvester");
//        Cat cat2 = new Cat("S");
//
//        // Invoking method which have an annotation
//        System.out.println("Single invocation");
//        for(Method method : Cat.class.getDeclaredMethods()) {
//            if(method.isAnnotationPresent(RunImmediately.class)) {
//                method.invoke(cat);
//            }
//        }
//
//        // Invoking methods which have annotation
//        // Invocation is done while using annotation parameters
//        System.out.println("Multiple invocation");
//        for(Method method : Cat.class.getDeclaredMethods()) {
//            if(method.isAnnotationPresent(RunImmediately.class)) {
//                RunImmediately annotation = method.getAnnotation(RunImmediately.class);
//
//                for(int i = 0; i < annotation.times(); i++) {
//                    method.invoke(cat);
//                }
//            }
//        }
//
//        // Retrieving fields which have annotation
//        for(Field field : cat.getClass().getDeclaredFields()) {
//            if(field.isAnnotationPresent(RunImmediately.class)) {
//                Object obj = field.get(cat);
//                System.out.println("Annotation present on field: " + field.getName());
//
//                if(obj instanceof String value) {
//                    System.out.println("Value of the field for object 'cat': " + value);
//                }
//            }
//        }
//    }
//}
package JAVA_ADVANCE.LEKSIONI_4_5.Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Async {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        Future<?> future = executor.submit(new MyTask("AsyncTask"));

        if (future.isDone()) {
            System.out.println("Task has completed");
        } else {
            System.out.println("Task is still running");
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (future.isDone()) {
            System.out.println("Task has completed");
        } else {
            System.out.println("Task is still running");
        }

        // Shutdown the ExecutorService
        executor.shutdown();
    }
}

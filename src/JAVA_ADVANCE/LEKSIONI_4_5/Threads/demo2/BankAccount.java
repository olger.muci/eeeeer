package JAVA_ADVANCE.LEKSIONI_4_5.Threads.demo2;
class BankAccount {
    private int balance;
    private final int overdraft;

    public BankAccount(int overdraft) {
        this.overdraft = overdraft;
    }

    public void credit(int amount) {
        balance += amount;
    }

    public void debit(int amount) {
        this.balance -= amount;
    }

    int getBalance() {
        return this.balance;
    }

    int getOverdraft() {
        return overdraft;
    }
}

class ATM {
    static void withdraw(BankAccount account, int amount) {
        int balance = account.getBalance();
        if((balance - amount) < - account.getOverdraft()) {
            System.out.println("Transaction denied");
        } else {
            account.debit(amount);
            System.out.println("$" + amount + " successfully withdrawn");
        }
        System.out.println("Current balance: " + account.getBalance());
    }
}

class Main {
    public static void main(String[] args) {
        BankAccount account = new BankAccount(50);

        account.credit(100);

        Thread t1 = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        ATM.withdraw(account, 100);
                    }
                }
        );

        Thread t2 = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        ATM.withdraw(account, 100);
                    }
                }
        );

        t1.start();
        t2.start();
    }
}
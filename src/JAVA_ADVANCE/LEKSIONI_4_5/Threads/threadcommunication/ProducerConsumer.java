package JAVA_ADVANCE.LEKSIONI_4_5.Threads.threadcommunication;
import java.util.LinkedList;
import java.util.Queue;

class ProducerConsumer {
    private final Queue<Integer> buffer = new LinkedList<>();
    private final int capacity = 5;

    public void produce() throws InterruptedException {
        int value = 0;
        while (true) {
            synchronized (this) {
                while (buffer.size() == capacity) {
                    // Buffer is full, wait for consumer to consume
                    wait();
                }

                System.out.println("Producer produced: " + value);
                buffer.add(value++);
                notify(); // Notify the consumer that new data is available

                Thread.sleep(1000); // Simulate some processing time
            }
        }
    }

    public void consume() throws InterruptedException {
        while (true) {
            synchronized (this) {
                while (buffer.isEmpty()) {
                    // Buffer is empty, wait for producer to produce
                    wait();
                }

                int value = buffer.poll();
                System.out.println("Consumer consumed: " + value);
                notify(); // Notify the producer that buffer is empty now

                Thread.sleep(1000); // Simulate some processing time
            }
        }
    }
}
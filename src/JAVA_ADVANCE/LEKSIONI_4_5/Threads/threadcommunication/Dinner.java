package JAVA_ADVANCE.LEKSIONI_4_5.Threads.threadcommunication;

public class Dinner {
    boolean isDinnerReady = false;

    public synchronized void waitForDinner() throws InterruptedException {
        while (!isDinnerReady) {
            wait();
        }
        System.out.println("Enjoy Your meal!");
    }

    public synchronized void prepareDinner() throws InterruptedException {
        System.out.println("Dinner preparing");
        isDinnerReady = true;
        Thread.sleep(5000);
        notify();
    }
}
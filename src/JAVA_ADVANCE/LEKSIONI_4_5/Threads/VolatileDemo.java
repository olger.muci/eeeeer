package JAVA_ADVANCE.LEKSIONI_4_5.Threads;

class WorkerThread extends Thread {
    private boolean stopRequested;

    @Override
    public void run() {
        int count = 0;
        while (!stopRequested) {
            count++;
        }
        System.out.println("WorkerThread terminated. Count: " + count);
    }

    public void requestStop() {
        stopRequested = true;
    }
}

public class VolatileDemo {
    public static void main(String[] args) throws InterruptedException {
        WorkerThread workerThread = new WorkerThread();
        workerThread.start();

        Thread.sleep(1000);

        workerThread.requestStop();
        System.out.println("Stop requested for WorkerThread");

        workerThread.join();

        System.out.println("Main thread completed");
    }
}
package JAVA_ADVANCE.LEKSIONI_4_5.Threads.threadinheritance;

class Runner extends Thread {
    @Override
    public void run() {
        for(int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("Hello " + i);
        }
    }
}
public class App {
    public static void main(String[] args) {
        Runner runner = new Runner();
        Runner runner2 = new Runner();

        runner.start();
        runner2.start();
    }
}
package JAVA_ADVANCE.LEKSIONI_4_5.USHTRIMI_4;

/**
 * Create a class called "Person" with properties:  name
 *                                                  age
 *                                                  address.
 * Create an ArrayList of Person objects and add some persons to it.
 *
 * Implement a method to search for a person by name and display their details.
 */
public class Person {
    private String name;
    private int age;
    private String address;

    public Person(String name, int age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}
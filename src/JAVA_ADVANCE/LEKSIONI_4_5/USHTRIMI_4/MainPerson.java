package JAVA_ADVANCE.LEKSIONI_4_5.USHTRIMI_4;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainPerson {
    static List<Person> people = new ArrayList<>();

    public static void main(String[] args) {
            // krijojme disa elemente
        Person p1 = new Person("Name 1", 15, "Address 1");
        Person p2 = new Person("Name 2", 20, "Address 2");
        Person p3 = new Person("Name 3", 25, "Address 3");
        Person p4 = new Person("Name 4", 30, "Address 4");

        List<Person> personList = new ArrayList<>();
            // shtojme elementet ne liste
        personList.addAll(Arrays.asList(p1,p2,p3,p4));

        System.out.println(personList);

        System.out.println(searchPerson("Name 4"));
    }
            // krijojme metoden qe ben kerkimin e personave sipas emrit dhe me pas kthen gjithe detajet e tij
    public static Person searchPerson(String name) {
        for(Person person : people) {
            if(person.getName().equals(name)) {
                return person;
            }
        }
        return null;
    }
}
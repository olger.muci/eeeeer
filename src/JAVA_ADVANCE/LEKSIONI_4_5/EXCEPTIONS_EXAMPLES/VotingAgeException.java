package JAVA_ADVANCE.LEKSIONI_4_5.EXCEPTIONS_EXAMPLES;

public class VotingAgeException extends Exception{
    public VotingAgeException(String error){
        super(error);
    }
}
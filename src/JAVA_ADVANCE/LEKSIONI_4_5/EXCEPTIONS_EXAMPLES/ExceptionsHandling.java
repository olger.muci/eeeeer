package JAVA_ADVANCE.LEKSIONI_4_5.EXCEPTIONS_EXAMPLES;

public class ExceptionsHandling {
    public static void main(String[] args) {
        int[] array = {1,2,3};

        // Exception never reached
        try {
            array[0] = 10;

            // Statement is reached
            System.out.println("Array is now changed");

            // Exception never raised
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }

        // Single exception
        try {
            array[3] = 5;

            // Statement is never reached
            System.out.println("Array is now changed");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }

        // Multiple exceptions
        try {
            array[2] = 5;
            int a = Integer.parseInt("a");

            // Statement is never reached
            System.out.println("Array is now changed");
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

        // Raising custom exceptions
        try {
            int age = Integer.parseInt("18");

            if(age < 18) {
                throw new VotingAgeException("Perdoruesi eshte nen moshe!!");
            }
        } catch (VotingAgeException e) {
            System.out.println(e.getMessage());
        }

        // Statement always reached as long as exceptions are caught
        System.out.println("Hello");
    }
}
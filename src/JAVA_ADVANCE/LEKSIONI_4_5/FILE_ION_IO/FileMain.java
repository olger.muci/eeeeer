package JAVA_ADVANCE.LEKSIONI_4_5.FILE_ION_IO;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileMain {
    public static void main(String[] args) throws IOException {
//        FileInputStream in = new FileInputStream("user.txt");
//        FileOutputStream out = new FileOutputStream("user_output.txt");
//        int c;
//        while ((c = in.read()) != -1) {
//            if(c != 'H') {
//                out.write(c);
//            }
//        }

//        BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\gerard\\Desktop\\user.txt"));
//        BufferedWriter out = new BufferedWriter(new FileWriter("user_output.txt"));
//
//        String line;
//        while ((line = in.readLine()) != null) {
//            out.write(line);
//            out.write("\n");
//        }
//
//        in.close();
//        out.close();

        Path path = Paths.get("C:\\Users\\gerard\\Desktop\\data.txt");
        Files.createFile(path);
        Files.write(path, "I am typing".getBytes(), StandardOpenOption.WRITE);
        for (String line : Files.readAllLines(path)) {
            System.out.println(line);
        }
    }
}
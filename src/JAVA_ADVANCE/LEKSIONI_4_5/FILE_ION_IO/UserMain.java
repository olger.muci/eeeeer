package JAVA_ADVANCE.LEKSIONI_4_5.FILE_ION_IO;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserMain {
    public static void main(String[] args) throws IOException {
        User user1 = new User(1, "gerard", "abc123", "Gerard", "Rama", LocalDate.of(1980,1,1));
        User user2 = new User(2, "alex", "def456", "Alex", "Doe", LocalDate.of(1985,1,1));
        User user3 = new User(3, "jane", "ghi789", "Jane", "Doe", LocalDate.of(1990,1,1));

        List<User> users = Arrays.asList(user1, user2, user3);

        BufferedWriter out = new BufferedWriter(new FileWriter("user.txt"));

        String line;
        for(User user : users) {
            line = user.getId() + ";" +
                    user.getUsername() + ";" +
                    user.getPassword() + ";" +
                    user.getFirstName() + ";" +
                    user.getLastName() + ";" +
                    user.getDateOfBirth() + "\n";

            out.write(line);
        }

        out.close();

        BufferedReader in = new BufferedReader(new FileReader("user.txt"));

        List<User> readUsers = new ArrayList<>();
        while((line = in.readLine()) != null) {
            String[] userLine = line.split(";");

            User readUser = new User();

            readUser.setId(Integer.valueOf(userLine[0]));
            readUser.setUsername(userLine[1]);
            readUser.setPassword(userLine[2]);
            readUser.setFirstName(userLine[3]);
            readUser.setLastName(userLine[4]);
            readUser.setDateOfBirth(LocalDate.parse(userLine[5]));

            readUsers.add(readUser);
        }

        in.close();

        readUsers.forEach(System.out::println);

        readUsers.forEach(user -> {
            user.setId(3);
        });
    }
}
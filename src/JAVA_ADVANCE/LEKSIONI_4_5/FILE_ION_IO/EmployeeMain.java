package JAVA_ADVANCE.LEKSIONI_4_5.FILE_ION_IO;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EmployeeMain {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Employee employee = new Employee("Alex", "Tirana");

        FileOutputStream fileStream = new FileOutputStream("user.txt");
        ObjectOutputStream objStream = new ObjectOutputStream(fileStream);

        objStream.writeObject(employee);

        FileInputStream fileInputStream = new FileInputStream("user.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        Employee readEmployee = (Employee) objectInputStream.readObject();

        System.out.println(readEmployee);

    }
}
package JAVA_ADVANCE.LEKSIONI_4_5;
import java.util.EmptyStackException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Ushtrimi10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        try {
            System.out.print("Vendos moshen: ");
            int age = input.nextInt();

            System.out.println("Mosha: " + age);

            if(age >= 18) {
                System.out.println("Ti mund te votosh");
            } else {
//                System.out.println("Ti NUK mund te votosh");
                throw new Exception("Ti NUK mund te votosh");
            }
        } catch (InputMismatchException e) {
            e.printStackTrace();
//            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
package JAVA_ADVANCE.LEKSIONI_4_5.GENERICS_EXAMPLES;

public class Printer <T extends Number>{
    T value;

    public Printer(T value) {
        this.value = value;
    }

    public <V> void print(V object) {
        System.out.println("Value : " + value + ", parameter: " + object);
    }
}
package JAVA_ADVANCE.LEKSIONI_4_5.GENERICS_EXAMPLES;

public class MainGenerics {
    public static void main(String[] args) {
        Printer<Integer> stringPrinter = new Printer<>(5);
        stringPrinter.print("param integer");

        Printer<Double> doublePrinter = new Printer<>(4.5);
        doublePrinter.print("param double");

        MathOperations<Integer> op1 = new MathOperations<>(5);
        MathOperations<Double> op2 = new MathOperations<>(4.5);

        System.out.println(op1.absEqual(op2));
    }
}
package JAVA_ADVANCE.LEKSIONI_4_5.GENERICS_EXAMPLES;

public class MathOperations <T extends Number>{
    T value;

    public MathOperations() {
    }

    MathOperations(T value) {
        this.value = value;
    }

    boolean absEqual(MathOperations<?> object) {
        return Math.abs(value.doubleValue()) == Math.abs(object.value.doubleValue());
    }
}
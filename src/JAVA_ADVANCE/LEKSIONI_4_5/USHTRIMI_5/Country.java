package JAVA_ADVANCE.LEKSIONI_4_5.USHTRIMI_5;
/**
 * Create a class called "Country" with properties:     name
 *                                                      population
 *                                                      capital.
 *
 * Create an ArrayList of Country objects and add some countries to it.
 *
 * Display the details of all the countries and find the country with the highest population.
 */
public class Country {
    private String name;
    private long population;
    private String capital;

    public Country(String name, long population, String capital) {
        this.name = name;
        this.population = population;
        this.capital = capital;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", population=" + population +
                ", capital='" + capital + '\'' +
                '}';
    }
}
package JAVA_ADVANCE.LEKSIONI_4_5.USHTRIMI_5;
import java.util.ArrayList;
import java.util.List;

public class MainCountry {
    static List<Country> countries = new ArrayList<>();

    public static void main(String[] args) {
            // krijojme disa elemente
        Country country1 = new Country("Shqiperi",  2500000,    "Tirana");
        Country country2 = new Country("SHBA",      300000000,  "Washington DC");
        Country country3 = new Country("Gjermani",  80000000,   "Berlin");
        Country country4= new Country( "Itali",     78000000,   "Roma");
        Country country5= new Country( "Greqi",     25000000,   "Athina");
        Country country6= new Country( "Spanja",    65000000,   "Barcelona");
        Country country7= new Country( "Franca",    72000000,   "Paris");
        Country country8= new Country( "Rusia",     115000000,  "Moska");
            // shtojme disa elemente
        countries.add(country1);
        countries.add(country3);
        countries.add(country5);
        countries.add(country7);
            // printojme elementet e listes
        countries.forEach(System.out::println);
        System.out.println();
            // afishojme vendin me popullsi me te larte pasi e kemi gjetur me ane te metodes se meposhteme
        System.out.println("Shteti me popullsine me te madhe eshte: ");
        System.out.println(findHighestPopulationCountry());
    }
        // krijojme metoden qe gjen vendin me popullsi me te larte
    public static Country findHighestPopulationCountry() {
        Country highestPopulationCountry = null;
        double highestPopulation = -1;

        for (Country country : countries) {
            if(country.getPopulation() > highestPopulation) {
                highestPopulation = country.getPopulation();
                highestPopulationCountry = country;
            }
        }
        return highestPopulationCountry;
    }
}
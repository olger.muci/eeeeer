package JAVA_ADVANCE.LEKSIONI_4_5;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Ushtrimi9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int numri1 = 0;
        int numri2 = 0;

        try {
            System.out.print("Vendos numrin e pare: ");
            numri1 = input.nextInt();

            System.out.print("Vendos numrin e dyte: ");
            numri2 = input.nextInt();

            if(numri2 == 0) {
                throw new ArithmeticException("Nuk behet pjestimi me 0!!");
            }

            System.out.println("Numri 1: " + numri1 + "\nNumri 2: " + numri2);

            double pjestimi = (double) numri1/numri2;

            System.out.println(pjestimi);
        } catch (InputMismatchException e) {
            System.out.println("Ke vendosur nje vlere jo numerike");
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }


        System.out.println(numri1);
    }
}
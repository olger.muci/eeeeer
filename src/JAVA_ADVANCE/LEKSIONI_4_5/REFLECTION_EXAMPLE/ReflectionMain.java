package JAVA_ADVANCE.LEKSIONI_4_5.REFLECTION_EXAMPLE;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionMain {
    public static void main(String[] args) throws Exception {

        Cat cat1 = new Cat("Tomm", 1);
        Cat cat2 = new Cat("Jerry",2);

        // Getting method of a class
        Method method = Cat.class.getDeclaredMethod("greeting");

        // Making private methods accessible
        method.setAccessible(true);
        // Executing function
        method.invoke(cat2);

        // Getting field of a class
        Field field = Cat.class.getDeclaredField("age");

        // Making private field accessible
        field.setAccessible(true);
        // Getting object of from field
        Object obj = field.get(cat1);

        // Assigning object to instance class
        if(obj instanceof Integer age) {
            System.out.println(age);
        }
    }
}
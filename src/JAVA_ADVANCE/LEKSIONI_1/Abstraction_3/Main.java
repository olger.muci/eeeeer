package JAVA_ADVANCE.LEKSIONI_1.Abstraction_3;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.setDiameter(6);

        System.out.println(circle.calculateArea());

        System.out.println(circle.calculatePerimeter());
    }
}
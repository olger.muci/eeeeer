package JAVA_ADVANCE.LEKSIONI_1.Abstraction_3;

public class Circle extends Shape {
    private double diameter;

    Circle() {
        super("RRETH");
    }

    @Override
    public double calculateArea() {
        System.out.print("Syprina e rrethit eshte: ");
        return Math.PI * Math.pow(diameter/2, 2);
    }

    @Override
    public double calculatePerimeter() {
        System.out.print("Perimetri i rrethit eshte: ");
        return Math.PI*diameter;
    }

    public void setDiameter(double diameter) {      // me .this bejme te mundur qe ti japim vete nje vlere konkrete
        this.diameter = diameter;
    }
}
package JAVA_ADVANCE.LEKSIONI_1.Enums_8;

public class Main {
    public static void main(String[] args) {

        Chocolate myChocolate = Chocolate.SCHOGETTEN;

        System.out.println("Vleresimi per cokollaten e zgjedhur eshte: "+myChocolate.vleresim);

        System.out.println("Shija eshte: "+myChocolate.shija);
    }
}
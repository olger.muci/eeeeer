package JAVA_ADVANCE.LEKSIONI_1.Enums_8;

public enum Chocolate {

    MILKA(9,"shume e mire"),
    LACTA(8,"goxha e mire"),
    LINDT(10,"super e mire"),
    ALBENI(4,"hajt mo"),
    SCHOGETTEN (7,"e mire");
    final int vleresim;
    final String shija;
    Chocolate(int vleresim, String shija) {
        this.vleresim = vleresim;
        this.shija=shija;
    }
}
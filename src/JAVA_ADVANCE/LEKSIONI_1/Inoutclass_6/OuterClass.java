package JAVA_ADVANCE.LEKSIONI_1.Inoutclass_6;

public class OuterClass {
    public void heyOuter() {
        class LocalClass {
            public void heyLocal() {
                System.out.println("Hey Local...");
            }
        }

        LocalClass localclass = new LocalClass();
        localclass.heyLocal();
        System.out.println("Hey Outer");

        InnerClass inner2 = new InnerClass() {
          @Override
          public void heyInner() {
              System.out.println("Hello from inner2...");
          }
        };

        InnerClass inner3 = new InnerClass() {
            @Override
            public void heyInner() {
                System.out.println("Hello from inner3...");
            }
        };

        inner2.heyInner();
        inner3.heyInner();
    }

    public class InnerClass {
        public void heyInner() {
            System.out.println("Hey Inner...");
        }
    }
}
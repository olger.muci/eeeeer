package JAVA_ADVANCE.LEKSIONI_1.Inoutclass_6;

public class Main {
    public static void main(String[] args) {
//        OuterClass outer = new OuterClass();
//        outer.heyOuter();
//        OuterClass.InnerClass inner = outer.new InnerClass();
//        inner.heyInner();


        OuterClass outer = new OuterClass();
        outer.heyOuter();

        OuterClass.InnerClass inner = outer.new InnerClass();
        OuterClass.InnerClass inner2 = outer.new InnerClass();
        OuterClass.InnerClass inner3 = outer.new InnerClass();

    }
}

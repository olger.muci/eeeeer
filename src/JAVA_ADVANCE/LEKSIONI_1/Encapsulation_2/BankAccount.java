package JAVA_ADVANCE.LEKSIONI_1.Encapsulation_2;

public class BankAccount {
    public double balance;

    protected double getBalance() {
        return this.balance;
    }

    public double getBalanceLogged() {
        System.out.println("Po aksesoni balancen: " + this.balance);
        return this.balance;
    }
}
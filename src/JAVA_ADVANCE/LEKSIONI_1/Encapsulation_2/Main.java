package JAVA_ADVANCE.LEKSIONI_1.Encapsulation_2;

public class Main {
    public static void main(String[] args) {
        BankAccount account = new BankAccount();
        account.balance = 2000;

        System.out.println("Balanca juaj aktuale eshte: " + account.balance);

        account.balance = -500.0;

        System.out.println(account.getBalanceLogged());

        System.out.println("Balanca juaj e re eshte: " + account.balance);

    }
}       // nuk funksion
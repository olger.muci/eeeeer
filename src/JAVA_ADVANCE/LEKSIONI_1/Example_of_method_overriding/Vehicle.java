package JAVA_ADVANCE.LEKSIONI_1.Example_of_method_overriding;

public class Vehicle {
    void run(){
        System.out.println("Mjeti eshte ne levizje.");
    }
}

class Bike extends Vehicle{
    @Override
    void run(){
        System.out.println("Bicikleta po ecen ngadale.");
    }
    public static void main(String[] args){
        Bike newBike = new Bike();           //creating object
        newBike.run();                      //calling method
    }
}
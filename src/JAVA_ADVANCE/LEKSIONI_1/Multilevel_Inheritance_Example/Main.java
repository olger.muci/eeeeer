package JAVA_ADVANCE.LEKSIONI_1.Multilevel_Inheritance_Example;
class Animal{                           // shembull me trashegimi te shumfishte
    void eat(){
        System.out.println("Duke ngrene...");
    }
}
    // trashegojme klasen Animal
class Dog extends Animal{
    void bark(){
        System.out.println("Duke lehur...");
    }
}
    // klasa BabyDog trashegon klasen Dog e cila ka trasheguar klasen Animal
class BabyDog extends Dog{
    void weep(){
        System.out.println("Duke qare...");
    }
}

class TestInheritance2{
    public static void main(String[] args){
        BabyDog myBabyDog=new BabyDog();    // krijojme nje objekt qe te therrasim metodat qe kemi trasheguar klase pas klase
        myBabyDog.weep();
        myBabyDog.bark();
        myBabyDog.eat();
    }
}
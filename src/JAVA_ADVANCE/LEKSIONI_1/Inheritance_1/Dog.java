package JAVA_ADVANCE.LEKSIONI_1.Inheritance_1;

public class Dog extends Animal {
    private String breed;

    public Dog(String name) {
        super(name);
    }

    public Dog (String name, String breed) {
        super(name);                    // keyword super perodret kur trashegojme metoden nga klasa prind
        this.breed = breed;
    }

    public void makeSound() {
        System.out.println("Qeni ben woof.");
        System.out.println(this.name);
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}
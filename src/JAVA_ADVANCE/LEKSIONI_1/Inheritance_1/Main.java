package JAVA_ADVANCE.LEKSIONI_1.Inheritance_1;

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("Mace");
        cat.makeSound();

        Dog dog = new Dog("Qen");
        dog.makeSound();
        System.out.println(dog);
    }
}
package JAVA_ADVANCE.LEKSIONI_1.Inheritance_1;

public class Animal {
    protected String name;      // mire eshte qe fields e nje klase te jene "protected"

    public Animal() {           // construktor default sepse nuk ka marr asnje parameter
        System.out.println("Klasa Animal eshte default");
    }

    public Animal(String name) {
        System.out.println("Klasa Animal");
        this.name = name;
    }

    public Animal(String name, int age) {
        System.out.println("Klasa Animal");
        this.name = name;
    }

    public void makeSound() {                           // metode
        System.out.println("Kafsha ben nje zhurme");
    }
}
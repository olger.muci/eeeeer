package JAVA_ADVANCE.LEKSIONI_1.Single_Inheritance_Example;

    // klasa parent
public class Animal {
        // krijojme nje metode
    void eat(){
        System.out.println("eating...");
    }
}
class Dog extends Animal{    // krijojme nje kalse te re duke trasheguar keshtu gjithe atributet e klases parent
    void bark(){
        System.out.println("barking...");
    }
}
class TestInheritance{
    public static void main(String[] args){
        Dog myDog=new Dog();    // per te thirrur metodat e klasave duhet te krijojme nje objekt
        myDog.bark();          // i therrasim permes pikes(.)
        myDog.eat();
    }
}
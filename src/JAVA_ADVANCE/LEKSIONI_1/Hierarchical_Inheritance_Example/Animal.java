package JAVA_ADVANCE.LEKSIONI_1.Hierarchical_Inheritance_Example;

public class Animal {
    void eat(){
        System.out.println("Kafsha e zgjedhur eshte duke ngrene...");
    }
}
class Dog extends Animal{
    void bark(){
        System.out.println("Qeni eshte duke lehur...");
    }
}
class Cat extends Animal{
    void meow(){
        System.out.println("Macja eshte duke mjaullitur...");
    }
}
class TestInheritance3{
    public static void main(String args[]){
        Cat myCat=new Cat();
        myCat.meow();
        myCat.eat();
        // myCat.bark(); jep error sepse klasa Cat eshte trasheguar nga klasa Animal
    }
}
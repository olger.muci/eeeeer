package JAVA_ADVANCE.LEKSIONI_1.Polymorphism_5;

public class TvRemoteController implements RemoteController {
    @Override
    public void controlDevice(boolean enable) {
        if (enable)
            System.out.println("TV is ON.");
        else
            System.out.println("TV is OFF.");
    }

    @Override
    public void controlDevice(boolean enable, int time) {
    }

    @Override
    public void controlVolume() {
        System.out.println("TV volume po ndryshon.");
    }

    @Override
    public void controlChannels() {
        System.out.println("TV channels po ndryshojne.");
    }

    public void lowerVolume() {
        System.out.println("TV volume lowered.");
    }

    public void lowerVolume(int level) {
        System.out.println("TV volume lowered by " + level);
    }
}
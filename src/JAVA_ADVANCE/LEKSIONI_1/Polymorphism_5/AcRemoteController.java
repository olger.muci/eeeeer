package JAVA_ADVANCE.LEKSIONI_1.Polymorphism_5;

public class AcRemoteController implements RemoteController {

    private int value;

    @Override
    public void controlDevice(boolean enable) {
        if (enable)
            System.out.println("AC is ON.");
        else
            System.out.println("AC is OFF.");
    }

    @Override
    public void controlDevice(boolean enable, int time) {

    }

    @Override
    public void controlVolume() {
    }

    @Override
    public void controlChannels() {
    }

    public void swing() {
        System.out.println("Fletat po levizin");
    }
}
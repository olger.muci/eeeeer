package JAVA_ADVANCE.LEKSIONI_1.Polymorphism_5;

public interface RemoteController {
    void controlDevice(boolean enable);

    void controlDevice(boolean enable, int time);

    void controlVolume();

    void controlChannels();
}
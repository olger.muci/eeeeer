package JAVA_ADVANCE.LEKSIONI_1.Polymorphism_5;

public class Main {
    public static void main(String[] args) {
        RemoteController tvRemoteController = new TvRemoteController();

        RemoteController acRemoteController = new AcRemoteController();

        tvRemoteController.controlDevice(true);

        acRemoteController.controlDevice(false);

        acRemoteController.controlChannels();

        acFunction(tvRemoteController);
    }

    public static void acFunction(RemoteController remoteController) {
        if(remoteController instanceof AcRemoteController) {
            AcRemoteController controller = (AcRemoteController) remoteController;
            controller.swing();
        }
    }

}

    /**
     *  instaceof verifikon nese një objekt i caktuar është një instancë e një klase të caktuar
     *  para se të kryhen veprime specifike në bazë të ketij informacioni.
     */
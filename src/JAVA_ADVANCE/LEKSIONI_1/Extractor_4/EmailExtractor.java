package JAVA_ADVANCE.LEKSIONI_1.Extractor_4;

public class EmailExtractor extends Extractor {

    @Override
    public String getPattern() {
        return "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
    }

    @Override
    public String getProcess() {
        return "Email Extract";
    }
}

package JAVA_ADVANCE.LEKSIONI_1.Extractor_4;

public class NumberExtractor extends Extractor {
    @Override
    public String getPattern() {
        return "\\d{10}";
    }

    @Override
    public String getProcess() {
        return "Number Extract";
    }
}

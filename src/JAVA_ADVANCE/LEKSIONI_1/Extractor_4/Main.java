package JAVA_ADVANCE.LEKSIONI_1.Extractor_4;

public class Main {
    public static void main(String[] args) {
        NumberExtractor numberExtractor = new NumberExtractor();

        numberExtractor.sendExtraction("""
                Hello
                0682045738
                0682045736
                myName
                """);

        EmailExtractor emailExtractor = new EmailExtractor();

        emailExtractor.sendExtraction("""
                abc@gmail.com
                0682045738
                0682045736
                myName
                """);
    }

}
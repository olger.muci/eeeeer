package JAVA_ADVANCE.LEKSIONI_1.Extractor_4;
import java.util.regex.Pattern;
abstract class Extractor {

    public abstract String getPattern();

    public abstract String getProcess();

    private String[] splitText(String text) {
        String[] array = text.split("\n");
        return array;
    }

    private String extract(String text) {
        String result = "";
        String[] words = splitText(text);

        Pattern pattern = Pattern.compile(getPattern());

        for(String word : words) {
            if(pattern.matcher(word).matches()) {
                result += word + "\n";
            }
        }

        return result;
    }

    public void sendExtraction(String text) {
        System.out.println("Duke filluar " + getProcess() + "...");
        String result = extract(text);
        System.out.println("Duke procesuar...");
        System.out.println("Ekstrakti eshte: ");
        System.out.println(result);
    }
}
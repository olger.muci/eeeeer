package JAVA_ADVANCE.TASKS.Ushtrimi_2;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a method that takes a string list as a parameter, then returns that list sorted alphabetically
 * from Z to A case-insensitive.
 */
public class Ushtrimi_2 {
    public static void main(String[] args) {
        List<String> values = new ArrayList<>();
        values.add("EFG");
        values.add("ABC");
        values.add("BCD");
        values.add("RQS");
        values.add("abc");

        System.out.println("lista fillestare eshte: ");
        values.forEach(System.out::println);

        System.out.println("Lisa e renditur eshte: " );
        sortDescending(values).forEach(System.out::println);
    }
    public static List<String> sortDescending(List<String> list){
        list.sort((o1, o2) -> o2.compareToIgnoreCase(o1)); // "compare to ignore case" krahason por shmang rastin qe ka shkronja dhe kapitale dhe te vogla
        return list;
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_1;
import java.util.ArrayList;
import java.util.List;

/**
 * Create a method that takes a string list as a parameter, then returns the list sorted alphabetically from Z to A.
 */

public class Ushtrimi_1 {
    public static void main(String[] args) {
        List<String> values = new ArrayList<>();
        values.add("EFG");
        values.add("ABC");
        values.add("bBC");
        values.add("abc");
        values.add("DEF");
        values.add("CDE");
        values.add("BCD");

        System.out.println("Lista fillestare eshte: ");
        values.forEach(System.out::println);

        List<String> newList = sortDescending(values);

        System.out.println("\nLista e renditur");
        newList.forEach(System.out::println);
    }

    public static List<String> sortDescending(List<String> list) {
       list.sort((o1, o2) -> o2.compareToIgnoreCase(o1));          // duhet kuptuar mire
       return list;
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_4;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Create a Storage class that will have: a private Map field, a public constructor, and methods:
 *          addToStorage(String key, String value) → adding elements to the storage;
 *
 *          printValues(String key) → displaying all elements under a given key;
 *
 *          findValues(String value) → displaying all keys that have a given value.
 *
 * The Storage class should allow you to store multiple values under one key.
 */

public class Storage {
    public Map<String, List<String>> map = new HashMap<>();

    public Storage() {
    }

    public void addToStorage(String key, String value) {
        if(map.containsKey(key)) {
            map.get(key).add(value);
        } else {
            List<String> values = new ArrayList<>();
            values.add(value);
            map.put(key, values);
        }
    }

    public List<String> findValues(String value) {
        List<String> keys;

        keys = map.keySet()
                .stream()
                .filter(k -> map.get(k).contains(value))
                .collect(Collectors.toList());

        return keys;
    }

    public List<String> findValues2(String value) {
        List<String> keys = new ArrayList<>();

        for(String key : map.keySet()) {
            if(map.get(key).contains(value)) {
                keys.add(key);
            }
        }

        return keys;
    }

    public void printValues(String key) {
        if(key != null && map.containsKey(key))
            map.get(key)
                    .forEach(System.out::println);
    }
}
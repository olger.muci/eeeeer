package JAVA_ADVANCE.TASKS.Ushtrimi_4;
import java.util.List;


public class MainStorage {
    public static void main(String[] args) {
        Storage storage = new Storage();
        storage.addToStorage("Key1", "Value1");
        storage.addToStorage("Key1", "Value2");
        storage.addToStorage("Key1", "Value3");
        storage.addToStorage("Key2", "Value2");
        storage.addToStorage("Key2", "Value4");
        storage.addToStorage("Key3", "Value1");

        List<String> keys = storage.findValues("Value2");
        System.out.println(keys);

        storage.printValues("Key1");

    }
}
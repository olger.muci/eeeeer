package JAVA_ADVANCE.TASKS.Ushtrimi_23;

import JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_2.Employee;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Create a Zoo class that will have a collection of animals and will allow you to receive statistics about your
 * animals:
 * int getNumberOfAllAnimals() → returns the number of all animals.
 *
 * Map <String, Integer> getAnimalsCount() → returns the number of animals of each species.
 *
 * Map <String, Integer> getAnimalsCountSorted() → returns the number of animals of each species sorted based
 * on the number of animals of a given species, where the first element is always the species with the largest number of animals.
 *
 * void addAnimals(String, int) → adds n animals of a given species.
 */
public class Zoo {
    private Map<String,Integer> animals = new HashMap<>();
    public void addAnimals(String specie, Integer totalAnimals){
        try{
            animals.put(specie,animals.get(specie)+totalAnimals);
        }catch (Exception e){
            animals.put(specie,totalAnimals);
        }
    }
    public Integer getNumberOfAllAnimals(){
        Integer sum=0;
        for (Integer totalAnimalBySpecies:this.animals.values()){
            sum+=totalAnimalBySpecies;
        }
        return sum;
    }
    public Map<String ,Integer> getAnimalsCount(){
        return this.animals;
    }
    public Map<String,Integer> getAnimalsCountSorted(){
        Map<String,Integer> sortedList= new LinkedHashMap<>();
        this.animals.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEach(x ->sortedList.put(x.getKey(), x.getValue()));

        return sortedList;
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_23;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Zoo zoo= new Zoo();
        zoo.addAnimals("mammals", 21);
        zoo.addAnimals("birds", 16);
        zoo.addAnimals("reptiles", 32);
        zoo.addAnimals("mammals", 4);
        System.out.println("Total numbers of all animals is " + zoo.getNumberOfAllAnimals());

        System.out.println("Total animal for each specie");
        for (Map.Entry<String, Integer> element : zoo.getAnimalsCount().entrySet()) {
            System.out.println("Total number of animals for " + element.getKey()
                    + " is " + element.getValue());
        }

        System.out.println("Total animal for each specie sorted desc by value");
        for (Map.Entry<String, Integer> element : zoo.getAnimalsCountSorted().entrySet()) {
            System.out.println("Total number of animals for " + element.getKey()
                    + " is " + element.getValue());
        }
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_18;

public class ComputerMain {
    public static void main(String[] args) {
        Computer computer1= new Computer("Intel",8,"GeForce","DELL","Latitude");
        Computer computer2= new Computer("Intel",32,"ZOTAC","HP","Zbook");
        Computer computer3= new Computer("Amd",16,"Nvidia","HP","Elitebook");
        System.out.println(computer3.toString());
    }
}
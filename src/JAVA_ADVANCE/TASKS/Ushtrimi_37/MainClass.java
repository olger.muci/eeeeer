package JAVA_ADVANCE.TASKS.Ushtrimi_37;

import JAVA_ADVANCE.TASKS.Ushtrimi_36.ThreadPlaygroundRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Create a class containing the standard static method main and a variable of type Executor and using the
 * factory method newFixedThreadPool of the Executors class to create a pool of 2 executors.
 * In iteration, add 10 ThreadPlaygroundRunnable objects from the previous task to the executor. Use any
 * string and current iteration number as the name.
 */

public class MainClass {
    private static ExecutorService executor;

    public static void main(String[] args) throws InterruptedException {
        executor = Executors.newFixedThreadPool(2);

        for (int i = 1; i <= 10; i++) {
            Thread.sleep(1000);
            String name = "Thread " + i;
            ThreadPlaygroundRunnable runnable = new ThreadPlaygroundRunnable(name);
            executor.submit(runnable);
        }

        executor.shutdown();
    }
}
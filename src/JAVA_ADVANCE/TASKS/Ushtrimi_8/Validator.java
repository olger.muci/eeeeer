package JAVA_ADVANCE.TASKS.Ushtrimi_8;

public interface Validator {
    boolean validate(Parcel input);
}
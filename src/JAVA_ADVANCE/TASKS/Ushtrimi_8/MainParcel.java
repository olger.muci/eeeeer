package JAVA_ADVANCE.TASKS.Ushtrimi_8;

public class MainParcel {
    public static void main(String[] args) {
        Parcel parcel = new Parcel(30,3,300, 100, true);
        ParcelValidator parcelValidator = new ParcelValidator();

        boolean isParcelValid = parcelValidator.validate(parcel);

        if(isParcelValid) {
            System.out.println("Validation ended, parcel is valid.");
        } else {
            System.out.println("Validation ended, parcel is invalid.");
        }
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_8;

public class ParcelValidator implements Validator{
    @Override
    public boolean validate(Parcel input) {
        return isParcelDimesionsSumValid(input) &&
                                                    isParcelSizeValid(input) &&
                                                                                isParcelWeightValid(input);
    }

    private boolean isParcelDimesionsSumValid(Parcel input) {
        int dimensionsSum = input.getxLength() + input.getyLength() + input.getzLength();

        if(dimensionsSum > 300) {
            System.out.println("Size larger than 300.");

            return false;
        }

        return true;
    }

    private boolean isParcelSizeValid(Parcel input) {
        float minimumSize = 30F;

        if(input.getxLength() < minimumSize
                                            || input.getyLength() < minimumSize
                                                                                || input.getzLength() < minimumSize) {
            System.out.println("All sizes need to be at least 30");

            return false;
        }

        return true;
    }

    private boolean isParcelWeightValid(Parcel input) {
        if( input.getWeight() > 30 && !input.isExpress() ) {
            System.out.println("Non express parcels weight needs to be at max 30");

            return false;
        }

        if(input.getWeight() > 15 && input.isExpress()) {
            System.out.println("Express parcels weight needs to be at max 15");

            return false;
        }

        return true;
    }
}
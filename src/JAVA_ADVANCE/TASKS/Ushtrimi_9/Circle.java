package JAVA_ADVANCE.TASKS.Ushtrimi_9;

import JAVA_ADVANCE.TASKS.Ushtrimi_10.Movable;
import JAVA_ADVANCE.TASKS.Ushtrimi_10.MoveDirection;

public class Circle implements Movable {
    private Point2D center;
    private Point2D point;

    Circle(Point2D center, Point2D point) {
        this.center = center;
        this.point = point;
    }

    public double getRadius() {
        return Math.sqrt(
                            Math.pow(point.getX() - center.getX(), 2) +
                                                                        Math.pow(point.getY() - center.getY(), 2)
        );
    }

    public double getPerimeter() {
        return 2*Math.PI*getRadius();
    }

    public double getArea() {
        return Math.PI * Math.pow(getRadius(), 2);
    }

    @Override
    public void move(MoveDirection moveDirection) {

    }
}
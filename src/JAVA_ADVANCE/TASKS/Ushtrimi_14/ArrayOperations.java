package JAVA_ADVANCE.TASKS.Ushtrimi_14;
import java.util.*;

/**
 * Implement the following functionalities based on 100,000 element arrays with randomly selected values:
 *          1. return a list of unique items,
 *          2. return a list of elements that have been repeated at least once in the generated array,
 *          3. return a list of the 25 most frequently recurring items.
 * Implement a method that deduplicates items in the list. If a duplicate is found, it replaces it with a new
 * random value that did not occur before. Check if the method worked correctly by calling method number 2.
 */

public class ArrayOperations {
    public static void main(String[] args) {
        int[] array = generateRandomArray(100_000);

        List<Integer> uniqueItems = getUniqueItems(array);
        List<Integer> repeatedItems = getRepeatedItems(array);
        List<Integer> mostFrequentItems = getMostFrequentItems(array, 25);

        System.out.println("Unique Items:");
        System.out.println(uniqueItems);

        System.out.println("\nRepeated Items:");
        System.out.println(repeatedItems);

        System.out.println("\nMost Frequent Items:");
        System.out.println(mostFrequentItems);

        List<Integer> deduplicatedItems = deduplicateItems(repeatedItems, array);
        System.out.println("\nDeduplicated Repeated Items:");

        int[] a = new int[deduplicatedItems.size()];

        for(int i = 0; i < deduplicatedItems.size(); i++) {
            a[i] = deduplicatedItems.get(i);
        }

        System.out.println(getRepeatedItems(a));
    }

    public static int[] generateRandomArray(int size) {
        Random random = new Random();
        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt(size);
        }

        return array;
    }

    public static List<Integer> getUniqueItems(int[] array) {
        Set<Integer> set = new HashSet<>();

        for (int item : array) {
            set.add(item);
        }

        return new ArrayList<>(set);
    }

    public static List<Integer> getRepeatedItems(int[] array) {
        List<Integer> repeatedItems = new ArrayList<>();
        Set<Integer> uniqueItems = new HashSet<>();

        for (int item : array) {
            if (!uniqueItems.add(item)) {
                repeatedItems.add(item);
            }
        }

        return repeatedItems;
    }

    public static List<Integer> getMostFrequentItems(int[] array, int count) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();

        for (int item : array) {
            if(frequencyMap.containsKey(item)) {
                frequencyMap.put(item, frequencyMap.get(item) + 1);
            } else
                frequencyMap.put(item, 1);
        }

        List<Map.Entry<Integer, Integer>> entryList = new ArrayList<>(frequencyMap.entrySet());
//        entryList.sort((a, b) -> b.getValue() - a.getValue());
        entryList.sort(Comparator.comparingInt(Map.Entry<Integer,Integer>::getValue).reversed());

        List<Integer> mostFrequentItems = new ArrayList<>();

        for (int i = 0; i < 25; i++) {
            mostFrequentItems.add(entryList.get(i).getKey());
        }

        return mostFrequentItems;
    }

    public static List<Integer> deduplicateItems(List<Integer> items, int[] array) {
        Set<Integer> uniqueItems = new HashSet<>();
        Random random = new Random();

        for (int i = 0; i < items.size(); i++) {
            int item = items.get(i);

            if (!uniqueItems.add(item)) {
                int newIndex = random.nextInt(array.length);

                while (uniqueItems.contains(array[newIndex])) {
                    newIndex = random.nextInt(array.length);
                }

                items.set(i, array[newIndex]);
                uniqueItems.add(array[newIndex]);
            }
        }

        return items;
    }
}
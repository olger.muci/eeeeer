package JAVA_ADVANCE.TASKS.Ushtrimi_17;

/**
 * Create a ConversionType enum class with the constants:   METERS_TO_YARDS
 *                                                          YARDS_TO_METERS
 *                                                          CENTIMETERS_TO_ICHES
 *                                                          INCHES_TO_CENTIMETERS
 *                                                          KILOMETERS_TO_MILES
 *                                                          MILES_TO_KILOMETERS.
 *
 * Enum should have a Convertertype parameter used to perform calculations for a given type.
 *
 * Then create a MeasurementConverter class that will have the convert(int value, ConvertionType conversionType) method
 * and based on the value and type of conversion, used the Converter of the given type and returned the result.
 */
public class ConverterMain {
    public static void main(String[] args) {
        MeasurementConverter converter = new MeasurementConverter();
        double result = converter.convert(10, ConversionType.METERS_TO_YARDS);
        System.out.println(result);  // Output: 10.9361

        result = converter.convert(100, ConversionType.KILOMETERS_TO_MILES);
        System.out.println(result);  // Output: 62.1371

    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_17;

public enum ConversionType {
    METERS_TO_YARDS(new MeterToYardConverter()),
    YARDS_TO_METERS(new YardToMeterConverter()),
    CENTIMETERS_TO_INCHES(new CentimeterToInchConverter()),
    INCHES_TO_CENTIMETERS(new InchToCentimeterConverter()),
    KILOMETERS_TO_MILES(new KilometerToMileConverter()),
    MILES_TO_KILOMETERS(new MileToKilometerConverter());

    private ConverterType converter;

    ConversionType(ConverterType converter) {
        this.converter = converter;
    }
    public ConverterType getConverter() {
        return converter;
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_17;

public class InchToCentimeterConverter implements ConverterType {
    @Override
    public double convert(int value) {
        return value * 2.54;
    }
}
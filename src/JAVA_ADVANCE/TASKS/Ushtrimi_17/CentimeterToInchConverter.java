package JAVA_ADVANCE.TASKS.Ushtrimi_17;

public class CentimeterToInchConverter implements ConverterType {
    @Override
    public double convert(int value) {
        return value * 0.393701;
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_17;

public class MeasurementConverter {
    public double convert(int value, ConversionType conversionType) {
        return conversionType.getConverter().convert(value);
    }
}
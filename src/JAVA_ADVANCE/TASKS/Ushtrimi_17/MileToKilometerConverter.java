package JAVA_ADVANCE.TASKS.Ushtrimi_17;

public class MileToKilometerConverter implements ConverterType {
    @Override
    public double convert(int value) {
        return value * 1.60934;
    }
}
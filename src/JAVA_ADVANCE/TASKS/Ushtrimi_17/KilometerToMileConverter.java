package JAVA_ADVANCE.TASKS.Ushtrimi_17;

public class KilometerToMileConverter implements ConverterType {
    @Override
    public double convert(int value) {
        return value * 0.621371;
    }
}
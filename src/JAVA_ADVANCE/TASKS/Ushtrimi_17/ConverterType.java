package JAVA_ADVANCE.TASKS.Ushtrimi_17;

public interface ConverterType {
    double convert(int value);
}
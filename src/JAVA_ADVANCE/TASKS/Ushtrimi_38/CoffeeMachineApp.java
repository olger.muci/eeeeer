package JAVA_ADVANCE.TASKS.Ushtrimi_38;

/**
 * Write an application that will simulate a coffee making machine. In the event that any coffee brewing service
 * finds an empty water tank, the thread should stop. When the water is refilled in the machine, the thread
 * should be excited
 */
public class CoffeeMachineApp {
    public static void main(String[] args) {
        CoffeeMachine coffeeMachine = new CoffeeMachine();
        BrewingService service1 = new BrewingService(coffeeMachine, "Service 1");
        BrewingService service2 = new BrewingService(coffeeMachine, "Service 2");

        Thread thread1 = new Thread(service1);
        Thread thread2 = new Thread(service2);

        thread1.start();
        thread2.start();

        // Simulating water refill after some time
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        coffeeMachine.fillWaterTank(5);

        // Simulating program termination
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread1.interrupt();
        thread2.interrupt();
    }
}

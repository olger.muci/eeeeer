package JAVA_ADVANCE.TASKS.Ushtrimi_38;

public class CoffeeMachine {
    private int waterLevel = 0;

    public synchronized void fillWaterTank(int amount) {
        waterLevel += amount;
        System.out.println("Water tank filled. Current water level: " + waterLevel);
        notifyAll();
    }

    public synchronized void brewCoffee(String serviceName) {
        while (waterLevel <= 0) {
            try {
                System.out.println(serviceName + ": Water tank is empty. Waiting for water to be refilled.");
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return;
            }
        }
        System.out.println(serviceName + ": Brewing coffee. Water level: " + waterLevel);
        waterLevel--;
        System.out.println(serviceName + ": Coffee brewed successfully. Remaining water level: " + waterLevel);
    }
}

class BrewingService implements Runnable {
    private CoffeeMachine coffeeMachine;
    private String serviceName;

    public BrewingService(CoffeeMachine coffeeMachine, String serviceName) {
        this.coffeeMachine = coffeeMachine;
        this.serviceName = serviceName;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            coffeeMachine.brewCoffee(serviceName);
            try {
                Thread.sleep(1000); // Simulating coffee brewing time
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        System.out.println(serviceName + ": Stopping brewing service.");
    }
}


package JAVA_ADVANCE.TASKS.Ushtrimi_13;

import JAVA_ADVANCE.TASKS.Ushtrimi_12.Car;
import JAVA_ADVANCE.TASKS.Ushtrimi_12.EngineType;
import JAVA_ADVANCE.TASKS.Ushtrimi_12.Manufacturer;

import java.time.Year;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Create a CarService class that will contain a list of cars and implement the following methods:
 * 1. adding cars to the list,
 * 2. removing a car from the list,
 * 3. returning a list of all cars,
 * 4. returning cars with a V12 engine,
 * 5. returning cars produced before 1999,
 * 6. returning the most expensive car,
 * 7. returning the cheapest car,
 * 8. returning the car with at least 3 manufacturers,
 * 9. returning a list of all cars sorted according to the passed parameter: ascending / descending,
 * 10. checking if a specific car is on the list,
 * 11. returning a list of cars manufactured by a specific manufacturer,
 * 12. returning the list of cars manufactured by the manufacturer with the year of establishment <,>, <=,> =,
 * =,! = from the given.
 */
public class CarService {
    static List<Car> cars = new ArrayList<>();

    public static void main(String[] args) {

        Manufacturer manufacturerExercise12_1 = new Manufacturer("TOYOTA",
                1980,
                "JAPAN");

        Manufacturer manufacturerExercise12_2 = new Manufacturer("VOLKSWAGEN",
                2002,
                "GERMANY");

        Manufacturer manufacturerExercise12_3 = new Manufacturer("AUDI",
                2014,
                "USA");

        Manufacturer manufacturerExercise12_4 = new Manufacturer("PEUGEOT",
                2007,
                "FRANCE");

        Car car1= new Car("Benz",
                "S- class",
                30000,
                Year.of(1992),
                List.of(manufacturerExercise12_1,manufacturerExercise12_4),
                EngineType.V8);

        Car car2= new Car("PORSCHE",
                "CAYENE",
                29000,
                Year.of(2004),
                List.of(manufacturerExercise12_1,manufacturerExercise12_3,manufacturerExercise12_2),
                EngineType.S4);

        Car car3= new Car("VW",
                "POLO",
                1800,
                Year.of(2003),
                List.of(manufacturerExercise12_3,manufacturerExercise12_4),
                EngineType.S3);

        Car car4= new Car("AUDI",
                "TT",
                12500,
                Year.of(2007),
                List.of(manufacturerExercise12_3),
                EngineType.V6);

        Car car5= new Car("FIAT",
                "Punto",
                4500,
                Year.of(1997),
                List.of(manufacturerExercise12_2),
                EngineType.S3);

        Car car6= new Car("VOLKSWAGEN",
                "JETTA",
                6500,
                Year.of(2006),
                List.of(manufacturerExercise12_3,manufacturerExercise12_1),
                EngineType.V6);

        Car car7= new Car("PORSCHE",
                "911",
                15000,
                Year.of(2012),
                List.of(manufacturerExercise12_2,manufacturerExercise12_1),
                EngineType.V8);

        Car car8= new Car("BENZ",
                "SPRINTER",
                20000,
                Year.of(1997),
                List.of(manufacturerExercise12_1,manufacturerExercise12_2,manufacturerExercise12_4),
                EngineType.V8);

        addCarToList(car1);
        addCarToList(car5);
        addCarToList(car3);
        addCarToList(car7);
        System.out.println("most expensice car is: "+findMostExpensiveCar());


    }
    public static void addCarToList(Car car){               // metoda 1
        cars.add(car);                                      // metode e gateshme qe shton nje objekt ne liiste
    }

    public static void  removeCarFromList(Car car){         // metoda 2
        cars.remove(car); // metode e gatshme qe heq nga lista nje objekt
    }

    public static List<Car> findAllCars(){                  // metoda 3
        return cars;
    }

    public static List<Car> findAllV12Cars(){               // metoda 4
        return cars
                .stream()
                .filter(c ->c.getEngineType().equals(EngineType.V12))
                .collect(Collectors.toList());
    }

    public static  List<Car> findCarsBefore1999(){              // metoda 5
        return cars
                .stream()
                .filter(car -> car.getManufactureYear().isBefore(Year.of(1999)))
                .collect(Collectors.toList());

    }

    public static Car findMostExpensiveCar(){                       // metoda 6
        Car mostExpensiceCar = null;

        double price = -1;

                //   menyra 1 me cikel (funksionon)

       // for (JAVA_Fundamentals.JAVA_Fundamentals.Leksion1.Leksion1.Leksion4.CLASSES.BOOK.Car car:cars){
       //     if (car.getPrice() > price){
       //         price = car.getPrice();
       //         mostExpensiceCar = car;
       //     }
       // }
       // return mostExpensiceCar;


            // menyra 2 me stream (funksionon) (perparesi me stream)

        return cars
                .stream()
                .max(Comparator.comparing(Car::getPrice))
                .get();
    }

    public static List<Car> findCarsWithAtLeast3Manufactures(){             // metoda 8


        // menyra 1 (me cikel)

      //  List<JAVA_Fundamentals.JAVA_Fundamentals.Leksion1.Leksion1.Leksion4.CLASSES.BOOK.Car> tempCars= new ArrayList<>();
      //  for (JAVA_Fundamentals.JAVA_Fundamentals.Leksion1.Leksion1.Leksion4.CLASSES.BOOK.Car car:cars){ // "per secilen makine nga lista me makina "
      //      if (car.getManufacturerExercise10s().size()>2){
       //         tempCars.add(car)
         //   }
      //  }
      //  return tempCars;


        //  menyra 2 (me stream)


            return cars
                    .stream()
                    .filter(car -> car.getManufacturers().size()>2)
                    .collect(Collectors.toList());
    }

    public static boolean isCarPresentOnList(Car car){         // metoda 10
        return cars.contains(car); // metode e gatshme
    }

    public static List<Car> findCarsbymanufctures(Manufacturer manufacturerExercise12){        // metoda 11
        return cars
                .stream()
                .filter(car -> car.getManufacturers().contains(manufacturerExercise12))
                .collect(Collectors.toList());
    }
}
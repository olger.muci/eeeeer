package JAVA_ADVANCE.TASKS.Ushtrimi_24_dhe_25;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a Basket class that imitates a basket and stores the current number of items in the basket.
 *
 * Add the addToBasket() method, which adds the element to the basket (increasing the current state by 1) and the
 * removeFromBasket() method, which removes the element from the basket (reducing the current state by 1).
 *
 * The basket can store from 0 to 10 items. When a user wants to remove an element at 0 items state or add an
 * element at 10 items state, throw the appropriate runtime exception (BasketFullException or BasketEmptyException).
 */
public class Basket {
    private List<String> itemList = new ArrayList<>();
    public void addToBasket() throws BasketFullException{
        if (itemList.size()>=10){
            throw new BasketFullException("sorry,you cant add more than 10 items");
        }else {
            String item="test";
            itemList.add(item);
        }
    }
    public void removeFromBasket() throws BasketEmptyException{
        if (itemList.size()==0){
            throw new BasketEmptyException("sorry, you cant remove any items because list is empty already");
        }else {
            itemList.remove(itemList.size()-1);
        }
    }

}
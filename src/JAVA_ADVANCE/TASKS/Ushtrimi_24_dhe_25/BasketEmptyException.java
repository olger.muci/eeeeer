package JAVA_ADVANCE.TASKS.Ushtrimi_24_dhe_25;

public class BasketEmptyException extends Exception{
    public BasketEmptyException(String message){
        super(message);
    }
    public BasketEmptyException(Throwable throwable){
        super(throwable);
    }
    public BasketEmptyException(String message, Throwable throwable){
        super(message,throwable);
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_24_dhe_25;

public class BasketFullException extends Exception{
    public BasketFullException(String message){
        super(message);
    }
    public BasketFullException (Throwable throwable){
        super(throwable);
    }
    public BasketFullException(String message, Throwable throwable){
        super(message,throwable);
    }
}

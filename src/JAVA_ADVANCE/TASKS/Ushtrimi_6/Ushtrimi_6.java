package JAVA_ADVANCE.TASKS.Ushtrimi_6;

import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

/**
 * Create a method that accepts TreeMap and prints the first and last EntrySet in the console.
 */

public class Ushtrimi_6 {
    public static void main(String[] args) {
        Map<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(1, "E HENE");
        treeMap.put(2, "E MARTE");
        treeMap.put(3, "E MERKURE");
        treeMap.put(4, "E ENJTE");
        treeMap.put(5, "E PREMTE");
        treeMap.put(6, "E SHTUNE");
        treeMap.put(7, "E DJELE");

        printFirstAndLastEntrySet(treeMap);
    }

    public static <K, V> void printFirstAndLastEntrySet(Map<K, V> treeMap) {
        Optional<Map.Entry<K, V>> firstEntry;
        Map.Entry<K, V> lastEntry;

        firstEntry = treeMap
                            .entrySet()
                            .stream()
                            .findFirst();

        if(firstEntry.isPresent()) {
            System.out.print("First Entry: ");
            System.out.println("Key: " + firstEntry.get().getKey() + ", Value: " + firstEntry.get().getValue());
        }

        lastEntry = treeMap
                            .entrySet()
                            .stream()
                            .toList()
                            .get(treeMap.size() - 1);

        System.out.print("Last Entry: ");
        System.out.println("Key: " + lastEntry.getKey() + ", Value: " + lastEntry.getValue());
    }
}
package JAVA_ADVANCE.TASKS.Ushtrimi_21;

import JAVA_ADVANCE.TASKS.Ushtrimi_20.Shape;
import JAVA_ADVANCE.TASKS.Ushtrimi_22.Fillable;

/**
 * Create an abstract 3DShape class that extends the Shape class from the previous task.
 *
 * Add an additional method calculateVolume().
 *
 * Create Cone and Qube classes by extending the 3DShape class, properly implementing abstract methods.
 *
 * Verify the solution correctness.
 */

public abstract class Shape_3D extends Shape implements Fillable {
    abstract double calculateVolume();
    @Override
    public void fill(int watterPerLitter) {
        if(watterPerLitter > this.calculateVolume()) {
            System.out.println("will pour too much water into the figure and overflow,");
        } else if (watterPerLitter == this.calculateVolume()) {
            System.out.println("fill the figure with water to the brim");
        } else {
            System.out.println("not pouring enough water.");
        }
    }
}
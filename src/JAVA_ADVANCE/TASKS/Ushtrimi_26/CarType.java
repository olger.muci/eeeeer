package JAVA_ADVANCE.TASKS.Ushtrimi_26;

public enum CarType {
    COUPE,
    CABRIO,
    SEDAN,
    HATCHBACK;
}
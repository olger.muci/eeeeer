package JAVA_ADVANCE.TASKS.Ushtrimi_26;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {        // functional programming perdor streams
        List<Manufacturer> manufacturerList = new ArrayList<>();
        manufacturerList.add(new Manufacturer("Toyota", 1952,
                Arrays.asList(new Model("Yaris", 2007, Arrays.asList(new Car("Toyota yaris 1.4",
                                        "Economic", CarType.SEDAN),
                                new Car("Toyota Yaris 1.6", "Sport", CarType.CABRIO))),
                        new Model("Auris", 2007, Arrays.asList(new Car("Toyota Auris 1.4",
                                "Economic", CarType.SEDAN), new Car("Toyoto Auris 1.4", "Sport",
                                CarType.CABRIO))))
        ));
        manufacturerList.add(new Manufacturer("VolskWagen", 1917,
                Arrays.asList(new Model("Golf", 2020,
                        Arrays.asList(new Car("Golf 6", "Economic", CarType.SEDAN),
                                new Car("Golf 6", "Sport", CarType.CABRIO),
                                new Car("Golf 5", "Economic", CarType.SEDAN),
                                new Car("Golf4", "Economic", CarType.SEDAN))))
        ));
            // krijojme disa objekte te klases Car
        Car mercedes = new Car("MERCEDES-BENZ","S CLASS",CarType.COUPE);
        Car audi = new Car("AUDI","Q7",CarType.SEDAN);
        Car volkswagen = new Car("VOLKSWAGEN","POLO",CarType.HATCHBACK);
        Car bmw = new Car("BMW","X5",CarType.CABRIO);

        System.out.println(" kerkesa A. ");
        manufacturerList
                .stream()
                .flatMap(manufacturer -> manufacturer.getModels().stream())
                .forEach(System.out::println);

        System.out.println("kerkesa B. ");
        manufacturerList
                .stream()
                .flatMap(manufacturer -> manufacturer.getModels().stream())
                .flatMap(model -> model.getCars().stream()).forEach(System.out::println);

        System.out.println("kerkesa 7. ");
        manufacturerList
                .stream()
                .flatMap(manufacturer -> manufacturer.getModels().stream())
                .flatMap(model -> model.getCars().stream())
                .map(car -> car.getName()).distinct()     // distinct ben te mundur qe mos te afishohen disa here
                .forEach(System.out::println);

        System.out.println("kerkesa 11. ");
        manufacturerList
                .stream()
                .filter(manufacturer -> manufacturer.getYearOfCreation() % 2 == 0)
                .flatMap(manufacturer -> manufacturer.getModels().stream())
                .flatMap(model -> model.getCars().stream())
                .forEach(System.out::println);
    }
}
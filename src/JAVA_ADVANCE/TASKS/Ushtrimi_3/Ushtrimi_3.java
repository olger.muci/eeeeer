package JAVA_ADVANCE.TASKS.Ushtrimi_3;
import java.util.HashMap;
import java.util.Map;
/**
 * Create a method that takes the map as a parameter, where the key is string and the value number, and then
 * prints each map element to the console in the format: Key: <k>, Value: <v>.
 * There should be a comma at the end of every line except the last, and a period at the last.
 */

public class Ushtrimi_3 {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        // Shtojme elementet ne map (me ane te metodes .put() )
        map.put("BENZ", 10);
        map.put("AUDI", 9);
        map.put("BMW", 4);
        map.put("VOLKSWAGEN", 7);
        map.put("PORSCHE", 9);
        map.put("FIAT", 3);
        map.put("MASERATI", 10);
        map.put("TOYOTA", 11);

        int size = map.size();      // map.size() jep gjatesine e map-it
        int count = 1;
        for (Map.Entry<String,Integer> entry : map.entrySet()){     // bredhja neper elementet e map-it
            System.out.print("Key: "+entry.getKey()+", Value: "+entry.getValue());
            if (count < size){
                System.out.println(",");
            } else {
                    System.out.print(".");
            }
            count++;
        }
    }
}
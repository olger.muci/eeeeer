package JAVA_ADVANCE.TASKS.Ushtrimi_20;

public class Triangle extends Shape{
    private int a;
    private int b;
    private int c;
    private double semiperimeter=(a+b+c)/2;

    public Triangle(int a, int b, int c,double semiperimeter) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.semiperimeter=semiperimeter;
    }
    @Override
    int calculatePerimeter() {
        return a+b+c;
    }
    @Override
    double calculateArea() {
        return Math.sqrt(semiperimeter*(semiperimeter-a)*(semiperimeter-b)*(semiperimeter-c));
    }
}
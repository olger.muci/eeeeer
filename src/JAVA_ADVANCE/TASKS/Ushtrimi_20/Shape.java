package JAVA_ADVANCE.TASKS.Ushtrimi_20;

/**
 * Create an abstract Shape class with the abstract methods calculatePerimeter() for calculating the
 * perimeter and calculateArea() for calculating the area.
 *
 * Create Rectangle, Triangle, Hexagon classes, extending the Shape class, and implementing abstract methods accordingly.
 *
 * Verify the solution correctness.
 */
public abstract class Shape {
  abstract int calculatePerimeter();
  abstract double calculateArea();
}
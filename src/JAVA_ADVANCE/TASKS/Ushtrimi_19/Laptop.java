package JAVA_ADVANCE.TASKS.Ushtrimi_19;

import JAVA_ADVANCE.TASKS.Ushtrimi_18.Computer;

import java.util.Objects;

/**
 * Create a Laptop class extending the Computer class from the previous task.
 *
 * The Laptop class should additionally contain the battery parameter.
 *
 * Implement additional getters, setters, constructor and overwrite the toString(), equals() and hashcode()
 * methods accordingly.
 *
 * Use a reference to parent class methods.
 */

public class Laptop extends Computer {
    private Integer battery_level;
    public Laptop(String processor, Integer ram, String graphicsCard, String company, String model, Integer battery_level) {
        super(processor, ram, graphicsCard, company, model);
        this.battery_level = battery_level;
    }
    public Integer getBattery() {
        return battery_level;
    }
    public void setBattery(Integer battery_level) {
        this.battery_level = battery_level;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "processor='" + this.getProcessor() + '\'' +
                ", ram=" + this.getRam() +
                ", graphicsCard='" + this.getGraphicsCard() + '\'' +
                ", companyName='" + this.getCompany() + '\'' +
                ", model='" + this.getModel() + '\'' +
                ", batteryLevel='" + this.getBattery() + "%" + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Laptop laptop)) return false;
        if (!super.equals(o)) return false;
        return Objects.equals(battery_level, laptop.battery_level);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), battery_level);
    }
}
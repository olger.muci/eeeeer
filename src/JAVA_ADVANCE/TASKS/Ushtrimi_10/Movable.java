package JAVA_ADVANCE.TASKS.Ushtrimi_10;

public interface Movable {
    public void move(MoveDirection moveDirection);
}
package JAVA_ADVANCE.TASKS.Ushtrimi_12;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CarService {
    private static List<Car> cars = new ArrayList<>();

    public static void main(String[] args) {
        Manufacturer manufacturer = new Manufacturer(
                "Benz",
                1900,
                "Germany");
        Manufacturer manufacturer2 = new Manufacturer(
                "BMW",
                1920,
                "Germany");
        Manufacturer manufacturer3 = new Manufacturer(
                "Chevrolet",
                1900,
                "USA");
        Manufacturer manufacturer4 = new Manufacturer(
                "Toyota",
                1880,
                "Japan");
        Manufacturer manufacturer5 = new Manufacturer(
                "Nissan",
                1890,
                "Japan");

        Car car1 = new Car(
                "Benz",
                "C-Class",
                50_000,
                Year.of(1995),
                List.of(manufacturer, manufacturer2, manufacturer3),
                EngineType.V6);

        Car car2 = new Car(
                "BMW",
                "M4",
                80_000,
                Year.of(2010),
                List.of(manufacturer),
                EngineType.V8);

        Car car3 = new Car(
                "Audi",
                "A8L",
                150_000,
                Year.of(1998),
                List.of(manufacturer, manufacturer2, manufacturer3, manufacturer4),
                EngineType.V12);

        addCarToList(car1);
        addCarToList(car2);
        addCarToList(car3);

        System.out.println("\nAfter adding cars.");
        cars.forEach(System.out::println);

//        removeCarFromList(car2);
//
//        System.out.println("\nAfter removing a car.");
//        cars.forEach(System.out::println);

        System.out.println("\nV12 Engines");
        System.out.println(findAllV12Cars());

        System.out.println("\nPre 1999 cars");
        System.out.println(findPre1999Cars());

        System.out.println("\nMost expensive car");
        System.out.println(findMostExpensiveCar());

        System.out.println("\nCars with at least 3 manufacturers");
        System.out.println(findCarsWith3Manufacturers());

        System.out.println("\nIs car part of the list");
        System.out.println(isCarPresentInList(car2));

        System.out.println("\nIs manufacturer present in list");
        System.out.println(findCarsByManufacturer(manufacturer2));
    }

    public static void addCarToList(Car car) {
        cars.add(car);
    }

    public static void removeCarFromList(Car car) {
        cars.remove(car);
    }

    public static List<Car> findAllCars() {
        return cars;
    }

    public static List<Car> findAllV12Cars() {
        return cars
                .stream()
                .filter(c -> c.getEngineType().equals(EngineType.V12))
                .toList();
    }

    public static List<Car> findPre1999Cars() {
        return cars
                .stream()
                .filter(c -> c.getManufactureYear().isBefore(Year.of(1999)))
                .toList();
    }

    public static Car findMostExpensiveCar() {
//        Car mostExpensiveCar = null;
//        double price = -1;
//
//        for(Car car : cars) {
//            if(car.getPrice() > price) {
//                price = car.getPrice();
//                mostExpensiveCar = car;
//            }
//
//        }
//
//        return mostExpensiveCar;

        return cars
                .stream()
                .max(Comparator.comparing(Car::getPrice))
                .get();
    }

    public static Car findLeastExpensiveCar() {
//        Car mostExpensiveCar = null;
//        double price = -1;
//
//        for(Car car : cars) {
//            if(car.getPrice() > price) {
//                price = car.getPrice();
//                mostExpensiveCar = car;
//            }
//
//        }
//
//        return mostExpensiveCar;

        return cars
                .stream()
                .min(Comparator.comparing(Car::getPrice))
                .get();
    }

    public static List<Car> findCarsWith3Manufacturers() {
//        List<Car> tempCars = new ArrayList<>();
//
//        for(Car car : cars) {
//            if(car.getManufacturers().size() > 2) {
//                tempCars.add(car);
//            }
//        }
//
//        return tempCars;

        return cars
                .stream()
                .filter(car -> car.getManufacturers().size() > 2)
                .collect(Collectors.toList());
    }

    public static boolean isCarPresentInList(Car car) {
        return cars.contains(car);
    }

    public static List<Car> findCarsByManufacturer(Manufacturer manufacturer) {
//        List<Car> tempCars = new ArrayList<>();
//
//        for(Car car : cars) {
//            if(car.getManufacturers().contains(manufacturer)) {
//                tempCars.add(car);
//            }
//        }
//
//        return tempCars;

        return cars
                .stream()
                .filter(car -> car.getManufacturers().contains(manufacturer))
                .collect(Collectors.toList());

    }
}
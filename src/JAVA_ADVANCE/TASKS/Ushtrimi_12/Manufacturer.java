package JAVA_ADVANCE.TASKS.Ushtrimi_12;
import java.util.Objects;

/**
 * Create a Manufacturer class that will contain fields:    name;
 *                                                          year of establishment;
 *                                                          country.
 *
 * Include all necessary methods and constructor parameters.
 *
 * Implement the hashCode() and equals() methods.
 */

public class Manufacturer {
    private String name;
    private int establishmentYear;
    private String country;

    public Manufacturer(String name, int establishmentYear, String country) {
        this.name = name;
        this.establishmentYear = establishmentYear;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEstablishmentYear() {
        return establishmentYear;
    }

    public void setEstablishmentYear(int establishmentYear) {
        this.establishmentYear = establishmentYear;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manufacturer that = (Manufacturer) o;
        return establishmentYear == that.establishmentYear && Objects.equals(name, that.name) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, establishmentYear, country);
    }

    @Override
    public String toString() {
        return "Manufacturer{" +
                "name='" + name + '\'' +
                ", establishmentYear=" + establishmentYear +
                ", country='" + country + '\'' +
                '}';
    }
}
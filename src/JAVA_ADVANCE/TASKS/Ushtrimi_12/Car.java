package JAVA_ADVANCE.TASKS.Ushtrimi_12;
import java.time.LocalDate;
import java.time.Year;
import java.util.List;
import java.util.Objects;

/**
 * Create a Car class that will contain fields: name;
 *                                              model;
 *                                              price;
 *                                              year of manufacture;
 *                                              manufacturer list (Manufacturer);
 *                                              engine type (represented as the enum class, e.g. V12, V8, V6, S6, S4, S3).
 *
 * Include all necessary methods and constructor parameters.
 *
 * Implement the hashcode() and equals() methods.
 */
public class Car {
    private String name;
    private String model;
    private double price;
    private Year manufactureYear;
    private List<Manufacturer> manufacturers;
    private EngineType engineType;

    public Car(String name,
               String model,
               double price,
               Year manufactureYear,
               List<Manufacturer> manufacturers,
               EngineType engineType) {
        this.name = name;
        this.model = model;
        this.price = price;
        this.manufactureYear = manufactureYear;
        this.manufacturers = manufacturers;
        this.engineType = engineType;
    }

    public Car(String name,
               String model) {
        this.name = name;
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Year getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(Year manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    public List<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(List<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.price, price) == 0 &&
                Objects.equals(name, car.name) &&
                Objects.equals(model, car.model) &&
                Objects.equals(manufactureYear, car.manufactureYear) &&
                Objects.equals(manufacturers, car.manufacturers) &&
                engineType == car.engineType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, model, price, manufactureYear, manufacturers, engineType);
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", manufactureYear=" + manufactureYear +
                ", manufacturers=" + manufacturers +
                ", engineType=" + engineType +
                '}';
    }
}
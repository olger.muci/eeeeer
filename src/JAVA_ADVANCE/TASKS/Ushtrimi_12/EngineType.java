package JAVA_ADVANCE.TASKS.Ushtrimi_12;

public enum EngineType {
    V4,
    V6,
    V8,
    V10,
    V12,
    V16,
    S3,
    S4;
}
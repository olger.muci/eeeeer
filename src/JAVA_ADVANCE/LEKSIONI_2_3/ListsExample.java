package JAVA_ADVANCE.LEKSIONI_2_3;

import java.util.ArrayList;
import java.util.List;

public class ListsExample {
    public static void main(String[] args) {

            // krijojme nje liste me elemente te tipit String (ne rastin tone)
        List<String> stringList = new ArrayList<>();
            // shtojme disa elemente ne liste
        stringList.add("Elementi 1");
        stringList.add("Elementi 2");
        stringList.add("Elementi 3");
        stringList.add("Elementi 4");

            // si te printojme gjithe elementet e listes
        System.out.println(stringList);

            // fshirja e nje elementi nga klasa me ane te komandes se gatshme "remove"
        stringList.remove("Elementi 2");

        System.out.println(stringList); // lista tani sdo e kete objektin "Elementi 2"

            // marrja e nje elementi te caktuar nga lista permes indexi
        System.out.println(stringList.get(1));

            // heqja e nje elementi te listes permes indexit; KUJTESE: indeximi fillon nga 0
        stringList.remove(0);
        System.out.println(stringList);

            // kontrollojme me ane te komandes "contains" nese nje element eshte pjese e listes ose jo
        System.out.println(stringList.contains("Elementi 4"));

            // marrja e indexit per nje element te caktuar me ane te komandes se gatshme "indexOf"
        System.out.println(stringList.indexOf("Elementi 4"));

            //fshirja e gjithe listes me ane te komandes se gatshme "clear"
        stringList.clear();

            // kontrollojme nese nje liste eshte boshe ose jo
        System.out.println(stringList.isEmpty());
    }
}
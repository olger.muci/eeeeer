package JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_2;
import java.util.ArrayList;
import java.util.List;

public class MainEmployee {
    public static void main(String[] args) {
        Employee e1 = new Employee("Name 1", 100, "Role 1");
        Employee e2 = new Employee("Name 2", 200, "Role 2");
        Employee e3 = new Employee("Name 3", 300, "Role 3");
        Employee e4 = new Employee("Name 3", 400, "Role 4");
        Employee e5 = new Employee("Name 5", 500, "Role 5");

        List<Employee> employees = new ArrayList<>();

        // shtimi i disa elementeve ne liste
        employees.add(e1);
        employees.add(e3);
        employees.add(e5);

        // printimi i gjithe elementeve te listes
        employees.forEach(System.out::println);

        // llogarisim rrogen mesatare dhe e afishojme ate

        double averageSalary = calculateAverageSalary(employees);
        System.out.printf("Rroga mesatare eshte: %.2f\n", averageSalary);

        // Employees equal based on name
        System.out.println(e1.equals(e4));
        System.out.println(e3.equals(e4));
    }

    // krijojme nje metode per te llogaritur rrogen mesatare
    public static double calculateAverageSalary(List<Employee> employees) {
        double sum = 0;

        for (Employee employee : employees) {
            sum = sum + employee.getSalary();
        }
        return sum / employees.size();
    }
}
package JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_2;
import java.util.Objects;

/**
 * Create a class called "Employee" with properties:   1) name
 *                                                     2) salary
 *                                                     3) role.
 *
 * Create an ArrayList of Employee objects and add some employees to it.
 *
 * Display the details of all the employees and calculate the average salary.
 */

public class Employee {
    private String name;
    private double salary;      // rroga
    private String role;        // pozicioni i punes

    public Employee(String name, double salary, String role) {
        this.name = name;
        this.salary = salary;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    // Employee instances are the same if they have the same name
    @Override
    public boolean equals(Object o) {           // nuk po e kuptoj
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Employee employee = (Employee) o;
        return Objects.equals(name, employee.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", role='" + role + '\'' +
                '}';
    }
}
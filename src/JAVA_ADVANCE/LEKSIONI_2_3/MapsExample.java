package JAVA_ADVANCE.LEKSIONI_2_3;


import JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_2.Employee;

import java.util.HashMap;
import java.util.Map;

public class MapsExample {
    public static void main(String[] args) {
            // krijojme disa instanca employee sipas ushtrimit 2 me siper
        Employee e1 = new Employee("Name 1", 100, "Role 1");
        Employee e2 = new Employee("Name 2", 200, "Role 2");
        Employee e3 = new Employee("Name 3", 300, "Role 3");
        Employee e4 = new Employee("Name 4", 400, "Role 4");

        Map<Integer, Employee> employees = new HashMap<>();    // Integer = KEY;    Employee = VALUE; specifike e MAP
            // shtojme disa elemente ne MAP permes metodes .put
        employees.put(1, e1);
        employees.put(2, e2);
        employees.put(3, e3);
        employees.put(4, e4);

            // Iterating through each key-value pair
        for(Map.Entry<Integer, Employee> employee : employees.entrySet()) {
            System.out.print("Key: " + employee.getKey());
            System.out.println(", Value: " + employee.getValue());
        }

            // Iterating through keys
        for (Integer key : employees.keySet()) {
            System.out.println("Key: " + key);
        }

            // Iterating through values
        for (Employee value : employees.values()) {
            System.out.println("Value: "+ value);
        }

            // Changing attributes of a value
        employees.get(4).setSalary(100000);
        System.out.println(employees.get(4));
    }
}
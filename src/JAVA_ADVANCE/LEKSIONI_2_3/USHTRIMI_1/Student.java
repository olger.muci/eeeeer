package JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_1;

/**
 * Create a class called "Student" with properties:     1) name
 *                                                      2) age
 *                                                      3) grade
 *
 * Create an ArrayList of Student objects and add some students to it.
 *
 * Display the details of all the students.
 */

public class Student {
    private String name;
    private int age;
    private int grade;

    public Student(String name, int age, int grade) {
        this.name = name;
        this.age = age;
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public int getGrade() {
        return grade;
    }
    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", grade=" + grade +
                '}';    }

}
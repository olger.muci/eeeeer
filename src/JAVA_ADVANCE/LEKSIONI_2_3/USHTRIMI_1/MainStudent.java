package JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainStudent {
    public static void main(String[] args) {
        Student student1 = new Student("Name 1", 18, 1);
        Student student2 = new Student("Name 2", 20, 2);
        Student student3 = new Student("Name 3", 24, 3);

        List<Student> students = new ArrayList<>();

        students.add(student1);
        students.add(student2);
        students.add(student3);

        // Print all students
        System.out.println(students);

        // Iterating over students

        // Method 1
        for(int i=0; i<students.size(); i++) {
            System.out.println(students.get(i));
        }

        // Method 2
        for(Student student : students) {
            System.out.println(student);
        }

        // Method 3
        students.forEach(System.out::println);

        // Method 4
        Iterator<Student> studentIterator = students.iterator();

        while(studentIterator.hasNext()) {
            System.out.println(studentIterator.next());
        }

        // Print students whose age is above 18
        for (Student student : students) {
            if (student.getAge() > 18) {
                System.out.println(student);
            }
        }

        // Print students whose grade is above 1
        for(Student student : students) {
            if (student.getGrade() > 1) {
                System.out.println(student);
            }
        }

    }
}
package JAVA_ADVANCE.LEKSIONI_2_3;

import JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_2.Employee;

import java.util.HashSet;
import java.util.Set;

public class SetsExample {
    public static void main(String[] args) {

        Set<Employee> employees = new HashSet<>();

        Employee e1 = new Employee("Name 1", 100, "Role 1");
        Employee e2 = new Employee("Name 2", 200, "Role 2");
        Employee e3 = new Employee("Name 3", 300, "Role 3");
        Employee e4 = new Employee("Name 4", 400, "Role 4");
            // shtimi i elementeve ne set
        employees.add(e1);
        employees.add(e2);
        employees.add(e3);
        employees.add(e4);

        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }
}
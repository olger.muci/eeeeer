package JAVA_ADVANCE.LEKSIONI_2_3;

import java.util.LinkedList;
import java.util.Queue;

public class QueuesExample {
    public static void main(String[] args) {
        Queue<String> futboll = new LinkedList<>();

        // shtimi i elementeve
        futboll.offer("Bayern Munchen");
        futboll.offer("Real Madrid");
        futboll.offer("Barcelona");
        futboll.offer("Juventus");

        System.out.println(futboll);

        // fshin dhe kthen elementin e pare te "radhes"
        System.out.println(futboll.poll());

        // kthen elementin e pare te "radhes" por nuk e fshin
        System.out.println(futboll.peek());
    }
}
package JAVA_ADVANCE.LEKSIONI_2_3;

import java.util.Stack;


public class StacksExample {
    public static void main(String[] args) {

        Stack<String> stack = new Stack<>();

            // Adding elements in the stack
        stack.push("Tomm");
        stack.push("Jerry");
            // afishojme elementet e "raftit"
        System.out.println(stack);

        // Returns and removes head of stack
        stack.pop();

        System.out.println(stack);

        // Returns head of stack but does not remove it
        System.out.println(stack.peek());
    }
}
package JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_3;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MainCar {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();

        // krijojme disa objekte
        Car car1 = new Car("BENZ", "B CLASS", 2000);
        Car car2 = new Car("BENZ", "A CLASS", 2001);
        Car car3 = new Car("BMW",  "X5",      2009);
        Car car4 = new Car("BMW",  "X3",      2011);
        Car car5 = new Car("AUDI", "Q7",      2014);
        Car car6 = new Car("AUDI", "A4",      2006);

        // shtojme disa prej objekteve ne liste
        cars.add(car2);
        cars.add(car4);
        cars.add(car6);

        // afishojme elementet e listes (ata qe kemi shtuar ne)
        cars.forEach(System.out::println);

        // rendisim listen ne rend rrites sipas vitit te prodhimit (te elementeve egzistues) dhe e afishojme
        cars.sort(new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.getYear().compareTo(o2.getYear());
            }
        });

        System.out.println("Renditja e re: ");
        cars.forEach(System.out::println);
    }
}
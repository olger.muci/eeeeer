package JAVA_ADVANCE.LEKSIONI_2_3.USHTRIMI_3;

/**
 * Create a class called "Car" with properties:     1) make
 *                                                  2) model
 *                                                  3) year
 *
 * Create an ArrayList of Car objects and add some cars to it.
 *
 * Display the details of all the cars and sort them by their year of manufacture.
 */

public class Car {
    private String make;
    private String model;
    private Integer year;

    public Car(String make, String model, Integer year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }
    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public Integer getYear() {
        return year;
    }
    public void setYear(Integer year) {
        this.year = year;
    }
    @Override
    public String toString() {
        return "Car{"
                + "make ='" + make
                + '\''
                + ", model ='" + model
                + '\''
                + ", year =" + year
                + "}";
    }
}
package JAVA_ADVANCE.LEKSIONI_2_3;
import java.util.List;
import java.util.Stack;

/**
 * Create a class called "BalancedParentheses" that checks whether a given string containing parentheses is balanced
 * or not using a stack.
 *
 * For example, "{[()]}" is balanced, while "{[(])}" is not.
 */

public class Ushtrimi6 {
    public static void main(String[] args) {
            // krijojme nje STACK
        Stack<String> stack = new Stack<>();
            // shtojme disa elemente ne STACK
        stack.push("{");
        stack.push("{");
        stack.push("[");
        stack.push("(");
        stack.push(")");
        stack.push("]");
        stack.push("}");
        stack.push("}");
            // afishojme elementet e STACK
        System.out.println(stack);

        int originalStackSize = stack.size();

        Stack<String> stackHolder = new Stack<>();
        for(int i=0; i<originalStackSize/2; i++) {
            stackHolder.push(stack.pop());
        }
        System.out.println(stack);
        System.out.println(stackHolder);

        List<String> parentheses = List.of("{", "}", "[", "]", "(", ")");

        boolean isBalanced = true;

        for(int i=0; i<originalStackSize/2; i++) {
            if(parentheses.contains(stack.peek()) && parentheses.contains(stackHolder.peek())) {
                if(stack.peek().equals("{")) {
                    if(!stackHolder.peek().equals("}")) {
                        isBalanced = false;
                        break;
                    }
                }
                if(stack.peek().equals("[")) {
                    if(!stackHolder.peek().equals("]")) {
                        isBalanced = false;
                        break;
                    }
                }
                if(stack.peek().equals("(")) {
                    if(!stackHolder.peek().equals(")")) {
                        isBalanced = false;
                        break;
                    }
                }
                stackHolder.pop();
                stack.pop();
            } else {
                System.out.println("Not correct stack");
                break;
            }
        }

        if(isBalanced) {
            System.out.println("Stack is balanced");
        } else {
            System.out.println("Stack not balanced");
        }
    }
}
// nuk po e kuptoj mire kerkesen e ushtrimit se cfare realisht kerkohet ne te vertete
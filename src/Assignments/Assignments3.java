package Assignments;

/**
 * Shkruaj programin ne java qe afishon strukturen e meposhteme:
 *
 *      ********
 *      *******
 *      ******
 *      *****
 *      ****
 *      ***
 *      **
 *      *
 *
 */
public class Assignments3 {
    public static void main(String[]args){
        for (int i=0;i<8;i++){
            for (int j=8;j>i;j--){
                System.out.print("$");
            }
            System.out.println();
        }
    }
}
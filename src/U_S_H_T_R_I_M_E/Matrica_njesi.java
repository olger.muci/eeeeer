package U_S_H_T_R_I_M_E;

public class Matrica_njesi {
    public static void main(String[] args) {
        int rrjesht, shtylla;
        boolean flag = true;
        int matrica[][] = {
                            {1, 0, 0},
                            {0, 1, 0},
                            {0, 0, 1}
        };
        rrjesht = matrica.length;
        shtylla = matrica[0].length;
        if(rrjesht != shtylla){
            System.out.println("Matrica duhet te jete matrice katrore, dmth me numer te njejte rrjeshtash dhe shtyllash. ");
        }
        else {
            for(int i = 0; i < rrjesht; i++){
                for(int j = 0; j < shtylla; j++){
                    if(i == j && matrica[i][j] != 1){
                        flag = false;
                        break;
                    }
                    if(i != j && matrica[i][j] != 0){
                        flag = false;
                        break;
                    }
                }
            }
            if(flag)
                System.out.println("Matrica e dhene eshte matrice njesi. ");
            else
                System.out.println("Matrica e dhene nuk eshte matrice njesi. ");
        }
    }
}
package U_S_H_T_R_I_M_E;

public class Matrica_e_transpozuar {
    public static void main(String[] args) {
        int matrica[][]={{1,3,4},{2,4,3},{3,4,5}};
        int transpose[][]=new int[3][3];
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                transpose[i][j]=matrica[j][i];
            }
        }
        System.out.println("Matrica fillestare eshte:");
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(matrica[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println("Matrica e transpozuar eshte:");
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(transpose[i][j]+" ");
            }
            System.out.println();
        }
    }
}
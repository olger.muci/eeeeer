package U_S_H_T_R_I_M_E;

public class Ushtrim_me_Switch {
    public static void main(String[] args) {
        int muaj=8;
        String monthString="";
        switch(muaj){
            case 1: monthString="1 - Janar";
                break;
            case 2: monthString="2 - Shkurt";
                break;
            case 3: monthString="3 - Mars";
                break;
            case 4: monthString="4 - Prill";
                break;
            case 5: monthString="5 - Maj";
                break;
            case 6: monthString="6 - Qershor";
                break;
            case 7: monthString="7 - Korrik";
                break;
            case 8: monthString="8 - Gusht";
                break;
            case 9: monthString="9 - Shtator";
                break;
            case 10: monthString="10 - Tetor";
                break;
            case 11: monthString="11 - Nentor";
                break;
            case 12: monthString="12 - Dhjetor";
                break;
            default:System.out.println("Nuk u gjet muaji!");
        }
        System.out.println(monthString);
    }
}
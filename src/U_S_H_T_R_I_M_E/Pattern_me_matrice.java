package U_S_H_T_R_I_M_E;

public class Pattern_me_matrice {
    public static void main(String[] args) {
        int n = 5;
        int [][] tab = new int [n][n];
        System.out.println();
        for ( int i = 0; i < n; i++) {
            tab[i][i] = i;
        }
        for( int i = 0; i<tab.length; i++){
            for (int j=0 ; j<tab[i].length; j++){
                System.out.print("        "+tab[i][j]);
            }
            System.out.println();
        }
    }
}

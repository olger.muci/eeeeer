package U_S_H_T_R_I_M_E;

public class Heqja_e_dublikatave_ne_nje_array {
    public static void main(String[] args) {
        int array[] = {10,20,20,30,30,40,50,50};
        int length = array.length;
        length = removeDuplicate(array, length);
        for (int i=0; i<length; i++)
            System.out.print(array[i]+" ");
    }
    public static int removeDuplicate(int Array[], int n){
        if (n==0 || n==1){
            return n;
        }
        int[] temp = new int[n];
        int j = 0;
        for (int i=0; i<n-1; i++){
            if (Array[i] != Array[i+1]){
                temp[j++] = Array[i];
            }
        }
        temp[j++] = Array[n-1];
        for (int i=0; i<j; i++){
            Array[i] = temp[i];
        }
        return j;
    }
}
package U_S_H_T_R_I_M_E;

public class Nr_char_ne_nje_string {
    public static void main(String[] args) {
        String text = "Hello! Une jam Enri Skenderaj.";
        int count = 0;
        for(int i = 0; i < text.length(); i++) {
            if(text.charAt(i) != ' ') // nuk i kemi llogaritur hapesirat
                count++;
        }
        System.out.println("Nr total i karaktereve ne stringun e dhene eshte " + count);
    }
}
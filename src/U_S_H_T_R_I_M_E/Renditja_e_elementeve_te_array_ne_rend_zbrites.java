package U_S_H_T_R_I_M_E;

public class Renditja_e_elementeve_te_array_ne_rend_zbrites {
    public static void main(String[] args) {
        int [] array = new int [] {5, 2, 8, 7, 1};
        int temp = 0;
        System.out.println("Elementet fillestar jane: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = i+1; j < array.length; j++) {
                if(array[i] < array[j]) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println();
        System.out.println("Elementet e renditur jane: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
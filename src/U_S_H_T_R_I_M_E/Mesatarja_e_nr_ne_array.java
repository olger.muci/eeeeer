package U_S_H_T_R_I_M_E;

import java.util.Scanner;

public class Mesatarja_e_nr_ne_array {
    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        int n=6;
        int shuma=0;
        int[]table= new int[n];
        for (int i=0;i<n;i++){
            System.out.print("Nr. "+(i+1)+": ");
            table[i]= input.nextInt();
        }
        for (int j=0;j< table.length;j++){
            shuma=shuma+table[j];
        }
        System.out.println("Mesatarja e numrave te dhene eshte: "+ (float)shuma/ table.length);
    }
}

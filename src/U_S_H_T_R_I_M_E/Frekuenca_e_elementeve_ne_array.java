package U_S_H_T_R_I_M_E;

public class Frekuenca_e_elementeve_ne_array {
    public static void main(String[] args) {
        int [] array = new int [] {1, 2, 8, 3, 2, 2, 2, 5, 1};
        int [] array_frekuencat = new int [array.length];
        int visited = -1;
        for(int i = 0; i < array.length; i++){
            int count = 1;
            for(int j = i+1; j < array.length; j++){
                if(array[i] == array[j]){
                    count++;
                    array_frekuencat[j] = visited;
                }
            }
            if(array_frekuencat[i] != visited)
                array_frekuencat[i] = count;
        }
        System.out.println("---------------------------------------");
        System.out.println("Elementi |   Frekuenca");
        System.out.println("---------------------------------------");
        for(int i = 0; i < array_frekuencat.length; i++){
            if(array_frekuencat[i] != visited)
                System.out.println("    " + array[i] + "    |    " + array_frekuencat[i]);
        }
        System.out.println("----------------------------------------");
    }
}